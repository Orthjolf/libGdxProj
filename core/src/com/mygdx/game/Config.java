package com.mygdx.game;

/**
 * Created by Wiliam on 10.08.2017.
 */

public class Config {
    private static Config instance;

    //public String tmp_worldPath = "test_world";
    public String tmp_worldPath = "the-file-name";

    public static synchronized Config get() {
        return instance == null ? new Config() : instance;
    }
}
