package com.mygdx.game.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.mygdx.game.Config;
import com.mygdx.game.JPlatformerGame;
import com.mygdx.game.input.GameInput;
import com.mygdx.game.utils.Constants;
import com.mygdx.game.utils.files.WorldLoader;
import com.mygdx.game.world.GameStage;
import com.mygdx.game.world.WorldController;
import com.mygdx.game.world.WorldRenderer;

/**
 * Игровой экран.
 * <p>
 * Created by Wiliam on 16.07.2017.
 */

public class GameScreen implements Screen {

	//tut
	Stage stage;
	BitmapFont font;
	Image splashImg;
	//end
	private WorldController worldController;
	private WorldRenderer worldRenderer;
	private GameStage gameStage;
	private GameInput gameInput;
	private Box2DDebugRenderer b2dr;

	public GameScreen() {
		worldController = new WorldController();
		worldRenderer = new WorldRenderer(worldController);
		gameStage = new GameStage(worldController);
		gameInput = new GameInput(gameStage);
		b2dr = new Box2DDebugRenderer();

		InputMultiplexer inputMultiplexer = new InputMultiplexer();
		inputMultiplexer.addProcessor(gameStage);
		inputMultiplexer.addProcessor(gameInput);

		Gdx.input.setInputProcessor(inputMultiplexer);

		//tut
		font = new BitmapFont();
		stage = new Stage(new FitViewport(900, 650, worldController.getWorldCamera()));
		//   Gdx.input.setInputProcessor(stage);

		Texture splashTex = new Texture(Gdx.files.internal("resources/images/Saw.png"));
		splashImg = new Image(splashTex);

		stage.addActor(splashImg);
		//end

		startGame(Config.get().tmp_worldPath);
	}


	// show
	//=====
	@Override
	public void show() {
		splashImg.setPosition(stage.getWidth() / 2 - 16, stage.getHeight() / 2 - 16);

		splashImg.addAction(Actions.sequence(
				Actions.alpha(0),
				Actions.parallel(
						Actions.moveBy(10, 100, 5),
						Actions.fadeIn(1f))
				)
		);
	}


	// startGame
	//==========

	/**
	 * Функция загружает мир из файла, и инициализирует запуск уровня. В случае, если
	 * загрузка не удалась, возвращает в главное меню.
	 *
	 * @param worldPath - путь к файлу мира
	 */
	private void startGame(String worldPath) {
		if (WorldLoader.loadWorld(worldPath, worldController)) {
			worldController.setLive(true);
			worldController.resetTimer();
			JPlatformerGame.get().setPaused(false);
		}
		else {
			JPlatformerGame.get().callScreen(new IntroScreen());
		}
	}


	// render
	//=======

	/**
	 * Обновление игровой логики и поэлементная отрисовка всех игровых объектов
	 *
	 * @param delta - дельта времени
	 */
	@Override
	public void render(float delta) {

		updateGame(delta);

		Gdx.gl.glClearColor(0f, 0f, 0f, 1);
		Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);


		Batch batch = JPlatformerGame.get().getBatch();
		batch.begin();
		{
			batch.setProjectionMatrix(worldController.getWorldCamera().combined);
			worldRenderer.render(batch);
			font.draw(batch, "1231231231", 120, 120);
		}
		batch.end();
		stage.draw();

		b2dr.render(worldController.getBox2dWorld(), worldController.getWorldCamera().combined
				.scl(Constants.PPM));
	}


	private void updateGame(float delta) {
		stage.act(delta);


		worldController.update(delta);
		gameInput.update();
	}

	// hide
	//=====
	@Override
	public void resize(int width, int height) {
		//  worldController.resizeCamera(width, height);
		stage.getViewport().update(width, height, true);
	}


	@Override
	public void hide() {
		this.dispose();
	}


	@Override
	public void dispose() {
		b2dr.dispose();
		gameStage.dispose();
		worldController.getBox2dWorld().dispose();
	}


	@Override
	public void pause() {
	}


	@Override
	public void resume() {

	}
}
