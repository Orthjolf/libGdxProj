package com.mygdx.game.Screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * ScreenTransition осуществляет плавный переход между экранами.
 * Обновление и рендеринг вызывается в методах render и update. Новый экран вызывается
 * при помощи метода init.
 *
 * @author Wilhelm Novitsky 09.07.2017
 */

public class ScreenTransition {
    // Состояния
    //==========
    public static final int STATE_NONE = 0;
    public static final int STATE_FADE_OUT = 1;
    public static final int STATE_SWITCH = 2;
    public static final int STATE_FADE_IN = 3;

    // Свойства
    //=========
    private int state;
    private Game game;
    private Screen nextScreen;

    // Рендеринг
    //==========
    private ShapeRenderer renderer;
    private OrthographicCamera camera;
    private float alpha;
    private int frameCounter;

    // Конструктор
    //============

    /**
     * newGame - объект типа Game, в который устанавливаются экраны screens.
     */
    public ScreenTransition(Game newGame) {
        // Свойства
        //=========
        game = newGame;

        // Рендеринг
        //==========
        renderer = new ShapeRenderer();
        camera = new OrthographicCamera();
    }


    // init
    //=====
    /**
     * Инициализирует переход на новый экран
     * screen - новый экран
     */
    public void init(Screen screen) {
        if (state == STATE_NONE) {
            state = STATE_FADE_OUT;
            nextScreen = screen;
            frameCounter = 0;
            alpha = 0f;
        }
    }


    // update
    //=======
    /**
     * Обновляет эффект перехода и ставит новый экран.
     */
    public void update() {
        // Угасание
        //=========
        if (state == STATE_FADE_OUT) {
            // Увеличение альфа канала (прозрачности)
            //=======================================
            alpha += Gdx.graphics.getDeltaTime() * 3f;

            // Смена состояния
            //================
            if (alpha >= 1f)
                state = STATE_SWITCH;
        }

        // Переключение экрана
        //====================
        else if (state == STATE_SWITCH) {
            // Меняем экран
            //=============
            if (nextScreen != null) {
                game.setScreen(nextScreen);
                nextScreen = null;
            }
            state = STATE_FADE_IN;
        }

        // Появление
        //==========
        else if (state == STATE_FADE_IN) {
            // Появление
            //==========
            if (frameCounter >= 2) {
                // Уменьшение прозрачности
                //========================
                alpha -= Gdx.graphics.getDeltaTime() * 3f;

                // Смена состояния
                //================
                if (alpha <= 0f) {
                    state = STATE_NONE;
                }
            } else
                // Счетчик кадров
                //===============
                frameCounter++;
        }
    }


    // draw
    //=====
    /**
     * Рисует эффект затухания, используя ShapeRenderer.
     */
    public void draw() {
        // Рисуем, если видимый
        //=====================
        if (alpha >= 0f) {
            // Включаем наложение
            //===================
            Gdx.gl.glEnable(GL20.GL_BLEND);

            // Рендерим шейп
            //==============
            renderer.begin(ShapeRenderer.ShapeType.Filled);
            renderer.setColor(0f, 0f, 0f, alpha);
            renderer.rect(0, 0, camera.viewportWidth, camera.viewportHeight);
            renderer.end();

            // Выключаем наложение
            //====================
            Gdx.gl.glDisable(GL20.GL_BLEND);
        }
    }


    // getState
    //=========
    public int getState() {
        return state;
    }


    // resize
    //=======
    public void resize(int width, int height) {
        // Обновляем камеру
        //=================
        camera.position.x = width / 2f;
        camera.position.y = height / 2f;
        camera.viewportWidth = width;
        camera.viewportHeight = height;
        camera.update();

        // Обновляем рендерер
        //===================
        renderer.setProjectionMatrix(camera.combined);
    }
}
