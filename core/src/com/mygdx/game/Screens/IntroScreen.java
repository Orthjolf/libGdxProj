package com.mygdx.game.Screens;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.mygdx.game.JPlatformerGame;


/**
 * Начальный экран. Показывает которкую вставку, после чего вызывает MenuScreen.
 *
 * @author Wilhelm novitsky 01.07.2017
 */
public class IntroScreen implements Screen {


    private ShapeRenderer shapeRenderer;
    public AssetManager assets;
    private OrthographicCamera gameCam;
    private long introTimer;

    public IntroScreen(){
        assets = new AssetManager();
        introTimer = System.currentTimeMillis() + 3000L;
        gameCam = new OrthographicCamera();
        shapeRenderer = new ShapeRenderer();
    }


    // show
    //=====
    @Override
    public void show() {

    }


    private void update(float delta){

    }

    // render
    //=======
    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(1f, 1f, 1f, 1);
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);

        update(delta);
        // Отрисовка лого
        //===============
        Batch batch = JPlatformerGame.get().getBatch();
        batch.begin();
        {
            batch.setProjectionMatrix(gameCam.combined);

        }
        batch.end();

        // Вызов экрана главного меню
        //===========================
        if (introTimer < System.currentTimeMillis() && introTimer != 0L) {
            introTimer = 0L;
            //    JPlatformerGame.get().callScreen(JPlatformerGame.get().menuScreen);
            JPlatformerGame.get().callScreen(new GameScreen());
        }
    }


    // hide
    //=====
    @Override
    public void resize(int width, int height) {
        // Обновление камеры
        //==================
        gameCam.viewportWidth = width;
        gameCam.viewportHeight = height;
        gameCam.update();
    }

    
    @Override
    public void hide() {
        this.dispose();
    }


    @Override
    public void dispose() {
        assets.dispose();
        shapeRenderer.dispose();
    }


    @Override
    public void pause() {
    }


    @Override
    public void resume() {

    }
}
