package com.mygdx.game.world;

import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Created by Wiliam on 10.08.2017.
 */

public class GameStage extends Stage {
    private WorldController worldController;

    public GameStage(WorldController worldController) {
        this.worldController = worldController;

    }

    public WorldController getWorldController() {
        return worldController;
    }
}