package com.mygdx.game.world.map.gameWorld;

import java.io.Serializable;

/**
 * Created by Wiliam on 09.08.2017.
 */

public class Tile implements Serializable {

    private int x;
    private int y;
    private int tileId;
    private int tileSetId;

    public Tile() {
        this.clear();
    }

    private void clear() {
        x = 0;
        y = 0;
        tileId = 0;
        tileSetId = 0;
    }
}
