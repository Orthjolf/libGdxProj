package com.mygdx.game.world.map.gameWorld;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Shape;
import com.mygdx.game.resources.ResourceRepository;

import java.util.ArrayList;

/**
 * Класс, описывающий игровой мир.
 * Содержит тайловую карту, объектную карту, набор бэкграундов, текстур и т.д.
 *
 * Created by Wiliam on 09.08.2017.
 */

public class GameWorld {

    // Части мира
    //===========
    public int tiles[][];

    public ArrayList<Vector2> visibleTiles;
    public ArrayList<Shape> tilesBodies;

    public int numCols;
    public int numRows;

    private Body body;

    private Rectangle mapBounds;

    public GameWorld() {
        mapBounds = new Rectangle(0, 0, 10 * 64, 10 * 64);
    }

    public void update() {

    }

    public void draw(Batch batch) {
        for (Vector2 tileCoords: visibleTiles){
            ResourceRepository.WORLD.dev_grey_rect.setX(tileCoords.y * 64);
            ResourceRepository.WORLD.dev_grey_rect.setY(tileCoords.x * 64);
            ResourceRepository.WORLD.dev_grey_rect.draw(batch);
        }
    }

    public Rectangle getMapBounds() {
        return mapBounds;
    }

    public int[][] getTiles() {
        return tiles;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public void setTiledMap(int[][] array) {
        tiles = array;
        numCols = array.length;
        numRows = array[0].length;
    }

    public void setStartArea(float posX, float posY, int i, int i1) {
    }

    public void setFinishArea(float x, float y, float width, float height) {
    }

    public void setVisibleTiles(ArrayList<Vector2> visibleTiles) {
        this.visibleTiles = visibleTiles;
    }

    public ArrayList<Vector2> getVisibleTiles() {
        return visibleTiles;
    }
}
