package com.mygdx.game.world.map;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Created by Wiliam on 06.08.2017.
 */

public class WorldShapeObject {
    static ChainShape shape;
    private Body body;

    public WorldShapeObject(World world) {
        BodyDef def = new BodyDef();

        def.type = BodyDef.BodyType.StaticBody;
        def.position.set(2, -8);
        def.fixedRotation = true;

        shape = new ChainShape();

        Vector2[] vertisies = new Vector2[]{
                new Vector2(0, 0),
                new Vector2(1, 4),
                new Vector2(2, 6),
                new Vector2(3, 7),
                new Vector2(4, 4),
                new Vector2(5, 7),
                new Vector2(6, 4),
                new Vector2(7, 0)
        };

        shape.createLoop(vertisies);


        FixtureDef fDef = new FixtureDef();
        fDef.density = 0.5f;
        fDef.friction = 1f;
        fDef.restitution = 0.7f;
        fDef.shape = shape;

        Body body;
        body = world.createBody(def);
        body.createFixture(fDef);

        shape.dispose();
    }
}
