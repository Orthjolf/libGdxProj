package com.mygdx.game.world.map.objects.concreteObjects;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.world.map.objects.MapObject;

/**
 * Created by Wiliam on 03.09.2017.
 */

public class Ground extends MapObject {

    public Ground(World world) {
        BodyDef def = new BodyDef();

        def.type = BodyDef.BodyType.StaticBody;
        def.position.set(0, 0);
        def.fixedRotation = false;

        ChainShape shape = new ChainShape();

        Vector2[] vertisies = new Vector2[]{
                new Vector2(-16, -16),
                new Vector2(16, -16),
                new Vector2(16, 50),
                new Vector2(-16, 50)
        };

        shape.createLoop(vertisies);

        Body body;
        body = world.createBody(def);
        body.createFixture(shape, 1.0f);
        shape.dispose();

        this.body = body;
    }
}
