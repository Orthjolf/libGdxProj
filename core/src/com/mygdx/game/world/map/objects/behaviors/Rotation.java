package com.mygdx.game.world.map.objects.behaviors;

import com.mygdx.game.world.map.objects.MapObject;

/**
 * Created by Wiliam on 05.09.2017.
 */

public class Rotation implements Behavior{

    @Override
    public void performAction(MapObject object) {
        object.sprite.setRotation(object.sprite.getRotation()+2f);
    }
}
