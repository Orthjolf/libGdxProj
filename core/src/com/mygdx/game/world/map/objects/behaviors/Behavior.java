package com.mygdx.game.world.map.objects.behaviors;

import com.mygdx.game.world.map.objects.MapObject;

/**
 * Created by Wiliam on 12.08.2017.
 */

public interface Behavior {
    void performAction(MapObject object);
}
