package com.mygdx.game.world.map.objects;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import static com.mygdx.game.utils.Constants.PPM;

/**
 * Created by Wiliam on 12.08.2017.
 */

public class MapObject1 {

    private Body body;
    private Sprite sprite;

    public MapObject1(int x, int y, int width, int height, boolean isStatic, World world) {
        body = createBox(x, y, width, height, isStatic, world);
    }

    public void setSprite(Sprite sprite){
        this.sprite = sprite;
    }

    private Body createBox(int x, int y, int width, int height, boolean isStatic, World world) {
        BodyDef def = new BodyDef();

        def.type = isStatic ? BodyDef.BodyType.StaticBody : BodyDef.BodyType.DynamicBody;
        def.position.set(x / PPM, y / PPM);
        def.fixedRotation = false;

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(width / PPM, height / PPM);

        Body pBody;
        pBody = world.createBody(def);
        pBody.createFixture(shape, 1.0f);
        shape.dispose();

        return pBody;
    }

    public Body getBody() {
        return body;
    }

    public void draw(Batch batch) {
       if(sprite !=null){
           sprite.setX(body.getPosition().x * PPM - 32);
           sprite.setY(body.getPosition().y * PPM - 32);
           sprite.setRotation((int) (body.getAngle() * 57.295780));
           sprite.draw(batch);
       }
    }

    public void punch(){
        body.applyForceToCenter(0, 1000, false);
        body.setAngularVelocity(50);
    }
}
