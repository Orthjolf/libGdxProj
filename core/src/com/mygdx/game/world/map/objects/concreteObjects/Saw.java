package com.mygdx.game.world.map.objects.concreteObjects;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.resources.ResourceRepository;
import com.mygdx.game.resources.Textures;
import com.mygdx.game.world.map.objects.MapObject;
import com.mygdx.game.world.map.objects.behaviors.Rotation;

import static com.mygdx.game.utils.Constants.PPM;

/**
 * Created by Wiliam on 07.09.2017.
 */

public class Saw extends MapObject {
	public Saw(World world) {

		BodyDef def = new BodyDef();

		def.type = BodyDef.BodyType.StaticBody;
		def.position.set(10, -10);
		def.fixedRotation = true;

		Shape shape = new CircleShape();
		shape.setRadius(42 / PPM);

		FixtureDef fDef = new FixtureDef();
		fDef.shape = shape;

		Body body;
		body = world.createBody(def);

		body.createFixture(fDef);

		shape.dispose();
		this.body = body;

		setBehavior(new Rotation());

		sprite = ResourceRepository.loadImage(Textures.saw, Textures.Saw);
		sprite.setX(body.getPosition().x * PPM - sprite.getWidth() / 2);
		sprite.setY(body.getPosition().y * PPM - sprite.getHeight() / 2);
	}

	@Override
	public void draw(Batch batch) {
		sprite.draw(batch);
	}

	@Override
	public void update() {
		behavior.performAction(this);
	}
}
