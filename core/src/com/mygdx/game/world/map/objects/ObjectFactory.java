package com.mygdx.game.world.map.objects;


import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.world.map.objects.concreteObjects.Ground;
import com.mygdx.game.world.map.objects.concreteObjects.Platform;
import com.mygdx.game.world.map.objects.concreteObjects.Player;

import java.io.Serializable;

/**
 * Created by Wiliam on 12.08.2017.
 */

public class ObjectFactory implements Serializable {
    private World world;

    public ObjectFactory(World world) {
        this.world = world;
    }

    public MapObject createPlayer() {
        return new Player(world);
    }

    public MapObject createPlatform(int x, int y, int width, int height) {
        return new Platform(x, y, width, height, world);
    }

    public MapObject createGround() {
        return new Ground(world);
    }
}
