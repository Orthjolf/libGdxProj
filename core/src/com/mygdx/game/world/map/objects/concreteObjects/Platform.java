package com.mygdx.game.world.map.objects.concreteObjects;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.world.map.objects.MapObject;

import static com.mygdx.game.utils.Constants.PPM;

/**
 * Created by Wiliam on 13.08.2017.
 */

public class Platform extends MapObject {

    public Platform(int x, int y, int width, int height, World world) {
        BodyDef def = new BodyDef();

        def.type = BodyDef.BodyType.StaticBody;
        def.position.set(x / PPM, y / PPM);
        def.fixedRotation = true;

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(width / PPM, height / PPM);

        Body body;
        body = world.createBody(def);
        body.createFixture(shape, 1.0f);
        shape.dispose();

        this.body = body;
    }
}
