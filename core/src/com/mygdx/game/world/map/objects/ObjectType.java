package com.mygdx.game.world.map.objects;

/**
 * Created by Wiliam on 12.08.2017.
 */

public enum ObjectType {
    Player,
    Box,
    Platform
}
