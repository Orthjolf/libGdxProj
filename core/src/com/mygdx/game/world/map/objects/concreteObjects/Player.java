package com.mygdx.game.world.map.objects.concreteObjects;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.MassData;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.animations.player.PlayerAnimationSet;
import com.mygdx.game.resources.ResourceRepository;
import com.mygdx.game.world.map.objects.MapObject;

import static com.mygdx.game.utils.Constants.PPM;

/**
 * Класс, описывающий главного персонажа
 *
 * Created by Wiliam on 13.08.2017.
 */

public class Player extends MapObject {

	public Predictor predictor;

	public Player(World world) {

		BodyDef def = new BodyDef();

		def.type = BodyDef.BodyType.DynamicBody;
		def.position.set(10, 20);
		def.fixedRotation = true;

		Shape shape = new CircleShape();
		shape.setRadius(42 / PPM);

		FixtureDef fDef = new FixtureDef();
		fDef.density = 1f;
		fDef.friction = 1f;
		fDef.restitution = 0f;
		fDef.shape = shape;

		MassData md = new MassData();
		md.mass = 1f;

		Body body;
		body = world.createBody(def);
		body.createFixture(fDef);
		body.setMassData(md);

		shape.dispose();

		this.body = body;

		animationSet = new PlayerAnimationSet();

		predictor = new Predictor();
	}

	public void jump(Vector2 force) {
		body.applyForceToCenter(force, true);
		animationSet.switchAnimation("jump");
		//  body.setAngularVelocity(30);
	}


	public void draw(Batch batch) {
		super.draw(batch);
		predictor.draw(batch);
	}

	@Override
	public void update() {
		if (body.getLinearVelocity().y == 0 &&
		    !animationSet.getCurrentAnimationName().equals("IdleAnimation"))
			animationSet.switchAnimation("idle");

		super.update();
		predictor.update();
	}

	public class Predictor {
		public final int numOfDots = 10;
		public final int dotsRange = 20;
		public Vector2[] dotsPositions;

		public Predictor() {
			dotsPositions = new Vector2[numOfDots];
			for (int i = 0; i < numOfDots; i++) {
				dotsPositions[i] = new Vector2(0,0);
			}
			ResourceRepository.WORLD.basketball.scale(-.6f);
		}

		public void draw(Batch batch) {
			for (int i = 0; i < numOfDots; i++) {
				ResourceRepository.WORLD.basketball.setPosition((int)dotsPositions[i].x,(int)dotsPositions[i].y);
				ResourceRepository.WORLD.basketball.draw(batch);
			}
		}

		public void update() {

		}
	}
}
