package com.mygdx.game.world.map.objects;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.mygdx.game.animations.AnimationSet;
import com.mygdx.game.world.WorldController;
import com.mygdx.game.world.map.objects.behaviors.Behavior;

import java.io.Serializable;

import static com.mygdx.game.utils.Constants.PPM;

/**
 * Абстракный класс, описывающий объект мира игры
 *
 * Created by Wiliam on 12.08.2017.
 */

public abstract class MapObject implements Serializable {

    public Sprite sprite;
    public Body body;
    protected AnimationSet animationSet;
    private boolean isLookingRight = true;
    private Vector2 position;
    protected Behavior behavior;

    public static MapObject createFromClassName(String className, WorldController worldController) {
        return null;
    }

    public void draw(Batch batch) {
        animationSet.drawAnimation(batch);
    }

    public void update() {
        animationSet.updateFrame();
        animationSet.setPosition(body.getPosition().x * PPM, body.getPosition().y * PPM);
    }

    public void setBehavior(Behavior behavior) {
        this.behavior = behavior;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public void setPosition(int x, int y) {
        this.position.x = x;
        this.position.y = y;
    }

    public Body getBody() {
        return body;
    }

    protected void setBody(Body body) {
        this.body = body;
    }

    public float getFrameSize() {
        return 0;
    }


    public void lookRight() {
        if (!isLookingRight)
            animationSet.flip();
        isLookingRight = true;
    }


    public void lookLeft() {
        if (isLookingRight)
            animationSet.flip();
        isLookingRight = false;
    }
}
