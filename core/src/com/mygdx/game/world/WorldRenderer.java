package com.mygdx.game.world;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.utils.ScissorStack;
import com.mygdx.game.resources.ResourceRepository;
import com.mygdx.game.resources.WorldTextureSet;
import com.mygdx.game.world.map.objects.MapObject;

import java.util.ArrayList;

/**
 * Created by Wiliam on 10.08.2017.
 */

public class WorldRenderer {

    private WorldController worldController;
    private ShapeRenderer shapeRenderer;
    private Rectangle scissors;

    private ArrayList<MapObject> mapObjects;

    public WorldRenderer(WorldController worldController) {
        this.worldController = worldController;

        mapObjects = worldController.getMapObjects();
        shapeRenderer = new ShapeRenderer();
        scissors = new Rectangle();

        WorldTextureSet.initializeTextures();
    }

    public void render(Batch batch) {

        ScissorStack.calculateScissors(worldController.getWorldCamera(),
                batch.getTransformMatrix(),
                worldController.getMapBounds(),
                scissors);

//        ScissorStack.pushScissors(scissors);
        {
            drawBackground(batch);
            drawMap(batch);
            drawMapObjects(batch);
            batch.flush();
        }
//        ScissorStack.popScissors();
    }

    private void drawBackground(Batch batch) {
        int bgWidth = ResourceRepository.WORLD.dev_grey_background.getRegionWidth();
        int bgHeight = ResourceRepository.WORLD.dev_grey_background.getRegionHeight();

        ResourceRepository.WORLD.dev_grey_background.setX(0);
        ResourceRepository.WORLD.dev_grey_background.setY(0);
        ResourceRepository.WORLD.dev_grey_background.draw(batch);

        ResourceRepository.WORLD.dev_grey_background.setX(bgWidth);
        ResourceRepository.WORLD.dev_grey_background.setY(0);
        ResourceRepository.WORLD.dev_grey_background.draw(batch);

        ResourceRepository.WORLD.dev_grey_background.setX(bgWidth);
        ResourceRepository.WORLD.dev_grey_background.setY(bgHeight);
        ResourceRepository.WORLD.dev_grey_background.draw(batch);
//
        ResourceRepository.WORLD.dev_grey_background.setX(0);
        ResourceRepository.WORLD.dev_grey_background.setY(bgHeight);
        ResourceRepository.WORLD.dev_grey_background.draw(batch);
    }

    private void drawMapObjects(Batch batch) {
        for (MapObject object : mapObjects)
            object.draw(batch);
    }

    private void drawMap(Batch batch) {
        ArrayList<Vector2> visibleTiles = worldController.getGameWorld().getVisibleTiles();
        for (Vector2 tileCoords: visibleTiles){
            ResourceRepository.WORLD.dev_grey_rect.setX(tileCoords.y * 64);
            ResourceRepository.WORLD.dev_grey_rect.setY(tileCoords.x * 64);
            ResourceRepository.WORLD.dev_grey_rect.draw(batch);
        }
    }


}
