package com.mygdx.game.world;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.camera.WorldCamera;
import com.mygdx.game.input.GameInput;
import com.mygdx.game.world.map.gameWorld.GameWorld;
import com.mygdx.game.world.map.gameWorld.Tile;
import com.mygdx.game.world.map.objects.MapObject;
import com.mygdx.game.world.map.objects.ObjectFactory;
import com.mygdx.game.world.map.objects.concreteObjects.Saw;

import java.io.Serializable;
import java.util.ArrayList;


/**
 * WorldController представляет игровой мир. Он объединяет все компоненты
 * (объекты, карту, время, очки и т.д.) и просчитывает их логику.
 *
 * Created by Wiliam on 16.07.2017.
 */

public class WorldController implements Serializable {

    public GameWorld gameWorld;
    public ArrayList<MapObject> mapObjects;
    public MapObject player, platform, saw;
    int a = 0;
    private World world;
    private WorldCamera gameCam;
    private ArrayList<Tile> visibleTiles;
    private ObjectFactory factory;


    public WorldController() {

        gameWorld = new GameWorld();
        gameWorld.visibleTiles = new ArrayList<Vector2>();

        world = new World(new Vector2(0, -9.8f), false);
        factory = new ObjectFactory(world);

        player = factory.createPlayer();
        // platform = factory.createGround();
        saw = new Saw(world);

        GameInput.setPlayer(player);

        gameCam = new WorldCamera(this);
        gameCam.setTarget(player, true);

        mapObjects = new ArrayList<MapObject>();
//        mapObjects.add(platform);
        mapObjects.add(player);
        mapObjects.add(saw);

        //   WorldShapeObject wso = new WorldShapeObject(world);

    }

    public void update(float delta) {
        if (Gdx.input.isKeyJustPressed(Input.Keys.RIGHT)) {
            switch (a) {
                case 0:
                    gameCam.setTarget(player, true);
                    break;
                case 1:
                    gameCam.setTarget(saw, false);
                    break;
                case 2:
                    gameCam.setTarget(new Vector2(4, 4), false);
                    break;
                case 3:
                    gameCam.setTarget(new Vector2(10, 10), true);
                    break;
                case 4:
                    a = -1;
                    break;
            }
            a++;
            System.out.println(a);
        }

        world.step(delta, 6, 2);
        for (MapObject object : mapObjects) {
            object.update();
        }
        //mapObjects.forEach(MapObject::update);

        gameCam.update();
    }

    // updateTileMap
    //==============

    public void updateTileMap() {

    }

    // updateObjectiveMap
    //===================
    public void updateObjectiveMap() {

    }

    // updateMapObjects
    //=================
    public void updateMapObjects() {

    }

    public void resizeCamera(int width, int height) {
        gameCam.resize(width, height);
    }

    private void updateTimers() {

    }

    private void updateWorldState() {

    }

    public void resetTimer() {
    }

    public void setLive(boolean live) {

    }

    public OrthographicCamera getWorldCamera() {
        return gameCam;
    }

    public Rectangle getMapBounds() {
        return gameWorld.getMapBounds();
    }

    public ArrayList<Tile> getVisibleTiles() {
        visibleTiles.clear();
        return visibleTiles;
    }

    public World getBox2dWorld() {
        return world;
    }

    public ArrayList<MapObject> getMapObjects() {
        return mapObjects;
    }

    public GameWorld getGameWorld() {
        return gameWorld;
    }

    public void setGameWorld(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
    }

    public Object getHoveredMapObject() {
        return null;
    }

    public void addMarkedMapObject(Object hoveredMapObject) {
    }

    public void setCells(Rectangle dragArea, Object selectedMapTile) {
    }

    public void removeMapObjects(Rectangle dragArea) {
    }

    public MapObject getPlayer() {
        return player;
    }

    public void setPlayer(MapObject player) {
        this.player = player;
    }

    public void removeMapObject(MapObject player) {
    }

    public void resetMapObjectRoutines() {
    }
}
