package com.mygdx.game.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.world.GameStage;
import com.mygdx.game.world.map.objects.MapObject;
import com.mygdx.game.world.map.objects.concreteObjects.Player;

import static com.mygdx.game.utils.Constants.PPM;


/**
 * Created by Wiliam on 10.08.2017.
 */

public class GameInput extends InputAdapter {
	private static Player player;
	GameStage gameStage;
	private Vector2 jumpStartPos;
	private Vector2 jumpReadyCoords;
	private Vector2 jumpForce;

	private InputController controller;

	public GameInput(GameStage gameStage) {
		this.gameStage = gameStage;

		jumpStartPos = new Vector2();
		jumpReadyCoords = new Vector2();
		jumpForce = new Vector2();
	}

	public static void setPlayer(MapObject newPlayer) {
		player = (Player) newPlayer;
	}


	public void update() {
		if (Gdx.input.isKeyPressed(Input.Keys.E)) {
			player.jump(new Vector2(100, 100));
		}

		int horizontalForce = 0;
		if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
			horizontalForce -= 1;
		}
		if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
			horizontalForce += 1;
		}
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		jumpStartPos.x = screenX;
		jumpStartPos.y = screenY;
		return true;
	}

	@Override
	public boolean keyDown(int key) {
		System.out.println(key);
		return true;
	}

	public boolean touchDragged(int screenX, int screenY, int pointer) {
		jumpReadyCoords.x = screenX;
		jumpReadyCoords.y = screenY;

		jumpForce.x = (jumpStartPos.x - jumpReadyCoords.x) * 10;
		jumpForce.y = -(jumpStartPos.y - jumpReadyCoords.y) * 10;

		for (int i = 0; i < player.predictor.numOfDots; i++) {
			player.predictor.dotsPositions[i].set(
					player.body.getPosition().x * PPM + jumpForce.x / 20 * i,
					player.body.getPosition().y * PPM + jumpForce.y / 20 * i
			);
		}

		if (jumpReadyCoords.x > jumpStartPos.y)
			player.lookLeft();
		else
			player.lookRight();

		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		player.jump(jumpForce);
		return true;
	}
}