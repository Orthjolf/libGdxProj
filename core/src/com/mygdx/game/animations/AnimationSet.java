package com.mygdx.game.animations;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Абстрактный класс набора анимаций.
 * Включает в себя спрайт лист и коллекцию, использующих его анимаций
 *
 * Created by Wiliam on 30.08.2017.
 */

public abstract class AnimationSet {

    protected Sprite spriteSheet;
    protected Vector2 numSpritesInSpriteSheet;
    protected Map<String, Animation> animationMap;
    protected Animation currentAnimation;
    protected ArrayList<Sprite> allFramesFromSpriteList;
    private Vector2 frameSize;

    // Конструктор
    //============
    protected AnimationSet() {
        frameSize = new Vector2();
        animationMap = new HashMap<String, Animation>();
    }


    // cutSpriteFrames
    //================

    /**
     * Функция для "нарезки" спрайт листа на отдельные кадры, в соответствии с из размером
     *
     * @param spriteSheet             - спрайт лист с кадрами анимации
     * @param numSpritesInSpriteSheet - количество кадров по вертикали и горезонтали
     *
     * @return - коллекция нарезанных кадров
     */
    protected ArrayList<Sprite> cutSpriteFrames(Sprite spriteSheet,
                                                Vector2 numSpritesInSpriteSheet) {

        frameSize.x = (spriteSheet.getWidth() / numSpritesInSpriteSheet.x);
        frameSize.y = (spriteSheet.getHeight() / numSpritesInSpriteSheet.y);
        ArrayList<Sprite> frames = new ArrayList<Sprite>();

        for (int y = 0; y < numSpritesInSpriteSheet.y; y++) {
            for (int x = 0; x < numSpritesInSpriteSheet.x; x++) {
                frames.add(new Sprite(spriteSheet,
                        (int) (x * frameSize.x),
                        (int) (y * frameSize.y),
                        (int) frameSize.x,
                        (int) frameSize.y));
            }
        }
        return frames;
    }


    // updateFrame
    //============
    public void updateFrame() {
        currentAnimation.updateFrame();
        if (currentAnimation.isOver) {
            currentAnimation.reset();
            switchAnimation("idle");
        }

    }

    // draw
    //=====
    public void drawAnimation(Batch batch) {
        currentAnimation.draw(batch);
    }


    public void setRotation(int rotation) {
        //   this.rotation = rotation;
    }

    // flip
    //=====

    /**
     * Зеркальное отражение кадров по горизонтали
     */
    public void flip() {
        for (Sprite frame : allFramesFromSpriteList)
            frame.flip(true, false); // TODO: 01.09.2017 оптимизировать!!
    }

    //setPosition
    //===========

    /**
     * Выставление позиции анимации
     */
    public void setPosition(float x, float y) {
        currentAnimation.setPosition(x, y);
    }

    //switchAnimation
    //============

    /**
     * Выставление текущей анимации
     */
    public void switchAnimation(String key) {
        currentAnimation = animationMap.get(key);
        //System.out.println("currentAnimation: " + currentAnimation + " ");
    }

    public String getCurrentAnimationName() {
        return currentAnimation.getClass().getSimpleName();
    }
}
