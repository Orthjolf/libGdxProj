package com.mygdx.game.animations.player;

import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.animations.AnimationSet;
import com.mygdx.game.resources.ResourceRepository;
import com.mygdx.game.resources.Textures;

/**
 * Анимация ничегонеделания персонажа
 *
 * Created by Wiliam on 30.08.2017.
 */

public class PlayerAnimationSet extends AnimationSet {

	public PlayerAnimationSet() {
		spriteSheet = ResourceRepository.loadImage(Textures.character, Textures.Character);
		numSpritesInSpriteSheet = new Vector2(4, 4);
		allFramesFromSpriteList = cutSpriteFrames(spriteSheet, numSpritesInSpriteSheet);

		animationMap.put("idle", new IdleAnimation(allFramesFromSpriteList));
		animationMap.put("jump", new JumpAnimation(allFramesFromSpriteList));

		currentAnimation = animationMap.get("idle");
	}
}
