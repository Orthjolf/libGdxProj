package com.mygdx.game.animations.player;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;

/**
 * Анимация прыжка персонажа
 *
 * Created by Wiliam on 03.09.2017.
 */

class JumpAnimation extends com.mygdx.game.animations.Animation {

    JumpAnimation(ArrayList<Sprite> allFrames) {
        super(allFrames);
        speed = 0.2f;
        offset = new Vector2(-70, -75);
        listOfFramesNumbers = new int[]{4, 5, 6, 7};
        frames = getNeededFrames(listOfFramesNumbers);
    }
}
