package com.mygdx.game.animations.player;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.animations.Animation;

import java.util.ArrayList;

/**
 * Анимация бездействия персонажа
 *
 * Created by Wiliam on 03.09.2017.
 */

class IdleAnimation extends Animation {

    IdleAnimation(ArrayList<Sprite> allFrames) {
        super(allFrames);
        speed = 0.2f;
        isLooped = true;
        offset = new Vector2(-70, -75);
        listOfFramesNumbers = new int[]{0, 1, 2, 3};
        frames = getNeededFrames(listOfFramesNumbers);
    }
}
