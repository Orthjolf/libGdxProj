package com.mygdx.game.animations;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;

/**
 * Абстрактный класс анимаций
 * Включает в себя разлинче параметры анимации, такие как
 * скорорость, зацикленность, список кадров для отрисовки и т.д.
 *
 * Created by Wiliam on 02.09.2017.
 */

public abstract class Animation {

    protected int[] listOfFramesNumbers;
    protected boolean isLooped;
    protected float speed;
    protected ArrayList<Sprite> frames;
    protected Vector2 offset;
    boolean isOver = false;
    private Vector2 position;  /* TODO: 01.09.2017 надо отрефакторить. По идее, анимация не
                                  TODO: должна ничего знать о своем расположении*/
    private float currentFrameTrigger;
    private int currentFrameNumber;
    private ArrayList<Sprite> allFrames;

    // Конструктор
    //============
    public Animation(ArrayList<Sprite> allFrames) {
        this.allFrames = allFrames;
        isLooped = false;
        speed = 1f;
        offset = new Vector2();
        position = new Vector2();
        currentFrameTrigger = 0;
        currentFrameNumber = 0;
    }


    // getNeededFrames
    //================

    /**
     * Фцнкция для выборки кадров для конкретной анимации
     *
     * @param listOfFramesIndexes - список нужных для конркетной анимации кадров
     *
     * @return - коллекция, содержащая выборку нужных кадров для конкретной анимации
     */
    protected ArrayList<Sprite> getNeededFrames(int[] listOfFramesIndexes) {
        ArrayList<Sprite> temp = new ArrayList<Sprite>();

        for (int index : listOfFramesIndexes)
            temp.add(allFrames.get(index));

        return temp;
    }

    // updateFrame
    //============
    void updateFrame() {

        currentFrameTrigger += speed;
        currentFrameNumber = (int) currentFrameTrigger;
        if (currentFrameNumber >= listOfFramesNumbers.length) {
            if (isLooped) {
                currentFrameNumber = 0;
                currentFrameTrigger = 0;
            }
            else {
                isOver = true;
            }
        }
    }

    // draw
    //=====
    public void draw(Batch batch) {

        frames.get(currentFrameNumber).setX(position.x);
        frames.get(currentFrameNumber).setY(position.y);
        frames.get(currentFrameNumber).draw(batch);
    }

    void reset() {
        isOver = false;
        currentFrameNumber = 0;
        currentFrameTrigger = 0;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }

    void setPosition(float x, float y) {
        position.set(x + offset.x, y + offset.y);
    }
}
