package com.mygdx.game.camera;

import com.mygdx.game.world.map.objects.MapObject;

import static com.mygdx.game.utils.Constants.PPM;

/**
 * Плавное следование камеры за объектом
 *
 * Created by Wiliam on 16.09.2017.
 */

class SmoothFollowTheTarget extends FollowTheTarget {

    private float interpolation = .1f;

    SmoothFollowTheTarget(WorldCamera camera, MapObject target) {
        super(camera, target);
    }


    @Override
    public void update() {
        position = camera.position;
        targetBodyPosition = target.body.getPosition().scl(PPM);
        position.x = camera.position.x + (targetBodyPosition.x - camera.position.x) * interpolation;
        position.y = camera.position.y + (targetBodyPosition.y - camera.position.y) * interpolation;
        camera.position.set(position);
    }
}
