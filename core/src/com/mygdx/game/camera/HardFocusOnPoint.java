package com.mygdx.game.camera;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;

/**
 * Статическая камера, выставленная по заданным координатам
 *
 * Created by Wiliam on 16.09.2017.
 */

class HardFocusOnPoint extends CameraBehavior {

    HardFocusOnPoint(Camera camera, Vector2 target) {
        super(camera);
        camera.position.set(target, 0);
    }


    @Override
    public void update() {
    }
}
