package com.mygdx.game.camera;

import com.badlogic.gdx.graphics.Camera;
import com.mygdx.game.world.map.objects.MapObject;

import static com.mygdx.game.utils.Constants.PPM;

/**
 * Жесткое следование камеры за объектам с привязкой к координатам
 *
 * Created by Wiliam on 16.09.2017.
 */

class HardFollowTheTarget extends FollowTheTarget {

    HardFollowTheTarget(Camera camera, MapObject target) {
        super(camera, target);
    }


    @Override
    public void update() {
        position = camera.position;
        targetBodyPosition = target.body.getPosition().scl(PPM);
        position.x = targetBodyPosition.x;
        position.y = targetBodyPosition.y;
        camera.position.set(position);
    }
}
