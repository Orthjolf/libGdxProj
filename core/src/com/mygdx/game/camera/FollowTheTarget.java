package com.mygdx.game.camera;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.world.map.objects.MapObject;

/**
 * Абстрактное поведение следования камеры за объектом
 *
 * Created by Wiliam on 16.09.2017.
 */

abstract class FollowTheTarget extends CameraBehavior {

    Vector2 targetBodyPosition;
    MapObject target;

    FollowTheTarget(Camera camera, MapObject target) {
        super(camera);
        this.target = target;
    }
}
