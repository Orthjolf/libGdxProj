package com.mygdx.game.camera;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.mygdx.game.world.WorldController;
import com.mygdx.game.world.map.objects.MapObject;

import java.io.Serializable;

/**
 * Класс, описывающий игровую камеру, и методы для работы с ней
 *
 * Created by Wiliam on 12.08.2017.
 */

public class WorldCamera extends OrthographicCamera implements Serializable {

    public float zoom;
    private WorldController worldController;
    private transient ScreenViewport screenViewport;
    private CameraBehavior cameraBehavior;

    public WorldCamera(WorldController worldController) {
        this.worldController = worldController;
    }

    @Override
    public void update() {
        cameraBehavior.update();
        super.update();
    }

    public void resize(int width, int height) {
        viewportWidth = width;
        viewportHeight = height;
        update();
    }

    public void zoomIn() {
    }

    public void zoomOut() {
    }

    public void setTarget(MapObject target, boolean smooth) {
        cameraBehavior = smooth ?
                new SmoothFollowTheTarget(this, target) :
                new HardFollowTheTarget(this, target);

    }

    public void setTarget(Vector2 target, boolean smooth) {
        cameraBehavior = smooth ?
                new SmoothFocusOnPoint(this, target) :
                new HardFocusOnPoint(this, target);
    }


    public void setCameraStyle(CameraBehavior cameraStyle) {

    }
}
