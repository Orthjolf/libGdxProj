package com.mygdx.game.camera;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;

import static com.mygdx.game.utils.Constants.PPM;

/**
 * Плавное движение камеры к конкретной точке
 *
 * Created by Wiliam on 16.09.2017.
 */

class SmoothFocusOnPoint extends CameraBehavior {

    Vector2 target;

    SmoothFocusOnPoint(Camera camera, Vector2 target) {
        super(camera);
        this.target = target.scl(PPM);
    }


    @Override
    public void update() {
        position = camera.position;
        position.x = camera.position.x + (target.x - camera.position.x) * .1f;
        position.y = camera.position.y + (target.y - camera.position.y) * .1f;
        camera.position.set(position);
    }
}
