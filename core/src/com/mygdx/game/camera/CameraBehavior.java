package com.mygdx.game.camera;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector3;

/**
 * Абстракный класс, описывающий поведение камеры
 *
 * Created by Wiliam on 16.09.2017.
 */

abstract class CameraBehavior {

    Camera camera;
    Vector3 position;

    CameraBehavior(Camera camera) {
        System.out.println(getClass().getSimpleName());
        this.camera = camera;
    }

    public abstract void update();
}
