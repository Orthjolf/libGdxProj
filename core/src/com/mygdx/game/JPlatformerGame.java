package com.mygdx.game;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.Screens.IntroScreen;
import com.mygdx.game.Screens.ScreenTransition;
import com.mygdx.game.utils.dev.Utils;

/**
 * Главный класс игры.
 *
 * @author Wilhelm Novitsky 01.06.2017
 */
public class JPlatformerGame extends Game {
    // Метаданные
    //===========
    private static final String TITLE = "EpicPlatformer";
    private static final String AUTHOR = "Wilhelm & Co";

    // Параметры
    //==========
    private static JPlatformerGame instance;
    private boolean paused;

    // Рендеринг
    //==========
    private SpriteBatch batch;
    private ScreenTransition screenTransition;


    // get
    //====

    /**
     * Создает одиночный экземпляр класса, если такого не существует, и возвращает его.
     */
    public static JPlatformerGame get() {
        if (instance == null)
            instance = new JPlatformerGame();
        return instance;
    }


    // create
    //=======
    @Override
    public void create() {
        // Заголовок
        //==========
        if (Gdx.app.getType() == Application.ApplicationType.Desktop) {
            Gdx.graphics.setTitle(
                    TITLE + " " + Utils.getLastVersion() +
                            " || "/* + Utils.getLineCountInFile()*/ +
                            " lines of fucking code");
        }

        // Загрузка конфига
        //=================
//        Config.get().initDefault();
//        Config.get().load();
//        Config.get().applyGraphics();

        // Рендер
        //=======
        batch = new SpriteBatch();
        screenTransition = new ScreenTransition(this);

        // Вызов нового экрана
        //====================
        callScreen(new IntroScreen());
        //callScreen(new GameScreen());
    }


    // render
    //=======
    @Override
    public void render() {
        // Очистка экрана
        //===============
        Gdx.gl20.glClearColor(0.07f, 0.075f, 0.08f, 1f);
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Рендер экрана
        //==============
        super.render();

        // Update + render transition
        //===========================
        screenTransition.update();
        screenTransition.draw();
    }


    // resize
    //=======
    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        screenTransition.resize(width, height);
    }


    // dispose
    //========
    @Override
    public void dispose() {
        // Save settings
        //==============
//        Config.get().save();

        batch.dispose();
        // ResourceRepository.get().dispose();
    }


    // getBatch
    //=========
    public SpriteBatch getBatch() {
        return batch;
    }


    // getScreenTransition
    //====================
    public ScreenTransition getScreenTransition() {
        return screenTransition;
    }


    // callScreen
    //===========

    /**
     * Вызов нового экрана
     */
    public void callScreen(Screen nextScreen) {
        screenTransition.init(nextScreen);
    }

    // isPaused
    //=========
    public boolean isPaused() {
        return paused;
    }

    // setPaused
    //==========
    public void setPaused(boolean value) {
        paused = value;
    }
}