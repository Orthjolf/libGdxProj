package com.mygdx.game.resources;

import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * Created by Wiliam on 12.10.2017.
 */

public class Textures {
	private final String path = "resources/images/";
	public static final String character = "dev/test_character_sprite.png";
	public static final String saw = "resources/images/Saw.png";

	public static Sprite Saw;
	public static Sprite Character;
}
