package com.mygdx.game.resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * Класс, который дает доступ к ресурсам, которые используются в игре.
 * Ресурсы расположенв во внутренних классах.
 *
 * Created by Wiliam on 16.07.2017.
 */

public class ResourceRepository {

	public static World WORLD = new World();


	/**
	 * Ленивая загрузка изображений
	 * @param path - путь к текстуре
	 * @param sprite - спрайт, который будет загружен
	 * @return - загруженный спрайт
	 */
	public static Sprite loadImage(String path, Sprite sprite) {
		if (sprite != null)
			return sprite;

		Texture temp = new Texture(Gdx.files.internal(path));
		temp.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
		sprite = new Sprite(temp);
		return sprite;
	}

	public void dispose() {

	}
}