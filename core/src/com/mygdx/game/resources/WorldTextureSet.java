package com.mygdx.game.resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * Created by Wiliam on 02.09.2017.
 */

public class WorldTextureSet {
    private static Texture texture_world_spritesheet;
    public static Sprite sprite_world_spritesheet;
    public static Sprite[] spriteList;

    public static void initializeTextures() {
        texture_world_spritesheet = new Texture(Gdx.files.internal("resources/images/test_world_spritesheet.png"));
        texture_world_spritesheet.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        sprite_world_spritesheet = new Sprite(texture_world_spritesheet);
    }

    public static void dispose() {
        texture_world_spritesheet.dispose();
    }
}
