package com.mygdx.game.resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * Created by Wiliam on 06.08.2017.
 */

public class World {

    // Текстуры
    //=========

    // Бэуграунды
    public Texture texture_dev_orange_back;
    public Texture texture_dev_lightGrey_back;

    public Texture texture_test_buttons;

    // Тайлсеты
    public Texture texture_dev_orange_spritesheet;
    public Texture texture_dev_grey_spritesheet;
    public Texture texture_dev_lightGrey_spritesheet;
    public Texture texture_dev_objects;



    // Спрайты
    //========

    // Бэкграунды
    public Sprite dev_grey_background;
    public Sprite dev_orange_background;

    // Элементы
    public Sprite dev_grey_rect;
    public Sprite dev_orange_rect;
    public Sprite dev_lightGrey_rect;
    public Sprite basketball;

    public Sprite dev_button_1;
    public Sprite dev_button_2;
    public Sprite dev_button_3;

    public World(){
        this.initDevTextures();
    }

    private void initDevTextures(){

        // Текстуры
        //=========
        texture_dev_lightGrey_back = new Texture(Gdx.files.internal("dev/dev_512_grey_back.jpg"));
        texture_dev_orange_back = new Texture(Gdx.files.internal("dev/dev_512_orange_back.jpg"));
        texture_dev_objects = new Texture(Gdx.files.internal("dev/objects.png"));

        texture_dev_grey_spritesheet = new Texture(Gdx.files.internal("dev/dev_grey_sprite_sheet.png"));
        texture_dev_lightGrey_spritesheet = new Texture(Gdx.files.internal("dev/dev_lightGrey_sprite_sheet.png"));
        texture_dev_orange_spritesheet = new Texture(Gdx.files.internal("dev/dev_orange_sprite_sheet.png"));

        texture_test_buttons = new Texture(Gdx.files.internal("dev/buttons.png"));
        // Фильтры
        //========
        texture_dev_lightGrey_back.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        texture_dev_orange_back.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        texture_dev_grey_spritesheet.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        texture_dev_lightGrey_spritesheet.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        texture_dev_orange_spritesheet.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        texture_dev_objects.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        texture_test_buttons.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        // Спрайты
        //========
        dev_grey_background = new Sprite(texture_dev_lightGrey_back);
        dev_orange_background = new Sprite(texture_dev_orange_back);

        dev_grey_rect = new Sprite(texture_dev_grey_spritesheet,288,128,64,64);
        dev_orange_rect = new Sprite(texture_dev_orange_spritesheet,288,128,64,64);
        basketball = new Sprite(texture_dev_objects,104,0,84,84);

        dev_button_1 = new Sprite(texture_test_buttons,0,0,64,64);
    }

    public void dispose() {
        texture_dev_lightGrey_back.dispose();
        texture_dev_orange_back.dispose();
        texture_dev_grey_spritesheet.dispose();
        texture_dev_lightGrey_spritesheet.dispose();
        texture_dev_orange_spritesheet.dispose();
        texture_dev_objects.dispose();

        texture_test_buttons.dispose();
    }
}
