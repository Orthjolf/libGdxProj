package com.mygdx.game.utils.files;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.mygdx.game.utils.dev.Utils;
import com.mygdx.game.world.WorldController;
import com.mygdx.game.world.map.gameWorld.GameWorld;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Класс, отвечающий за загрузку и считывание уровней из файлов
 *
 * Created by Wiliam on 24.08.2017.
 */


public class WorldLoader {

    private static final String FILE_EXTENSION = ".txt";
    private static final String FILE_DIR = "resources/worlds/";

    // loadWorld
    //==========

    /**
     * Считывает файл, парсит его, и превращает в уровень
     *
     * @param path            - путь к файлу
     * @param worldController - worldController для возвращаемого уровня
     *
     * @return - объект уровня
     */
    public static boolean loadWorld(String path, WorldController worldController) {

        path = FILE_DIR + path + FILE_EXTENSION;

        Loader loader =
                Gdx.app.getType() == Application.ApplicationType.Android ?
                        new AndroidWorldLoader(path) :
                        new PCWorldLoader(path);

        int numCols = loader.nextInt();
        int numRows = loader.nextInt();

        int[][] tiles = new int[numCols][numRows];



        for (int y = 0; y < numRows; y++) {
            for (int x = 0; x < numCols; x++) {
                tiles[y][x] = loader.nextInt();
                if (tiles[y][x] != 0)
                    System.out.print(tiles[y][x] + " ");
                else
                    System.out.print("  ");
            }
            System.out.println();
        }

        ArrayList<Vector2> visibleTiles = new ArrayList<Vector2>();
        for (int y = 0; y < numRows; y++) {
            for (int x = 0; x < numCols; x++) {
                if (tiles[y][x] != 0)
                    visibleTiles.add(new Vector2(y,x));
            }
        }



        CollisionGenerator collisionGenerator = new CollisionGenerator(tiles);

        Utils.startTimer();
        ArrayList<ChainShape> shapes = collisionGenerator.generateShapes();
        System.out.println(Utils.stopTimer());

        GameWorld gameWorld = new GameWorld();
        gameWorld.setTiledMap(tiles);
        gameWorld.setVisibleTiles(visibleTiles);


        for (ChainShape shape : shapes){
            BodyDef def = new BodyDef();

            def.type = BodyDef.BodyType.StaticBody;
            def.position.set(0, 0);
            def.fixedRotation = false;

            Body body;
            body = worldController.getBox2dWorld().createBody(def);
            body.createFixture(shape, 1.0f);
            shape.dispose();
        }
        worldController.setGameWorld(gameWorld);

        return true;
    }


    interface Loader {
        int nextInt();
    }


    private static class AndroidWorldLoader implements Loader {

        FileHandle file;
        String[] array;
        int indexInArray;

        AndroidWorldLoader(String path) {
            if (!Gdx.files.internal(path).exists()) {
                System.out.println(path + " не найден");
                return;
            }
            indexInArray = 0;

            file = Gdx.files.internal(path);
            String fileInStringFormat = file.readString();
            fileInStringFormat = fileInStringFormat.replace(" ", "-").replace("\n", "-");
            array = fileInStringFormat.split("-");
        }

        @Override
        public int nextInt() {
            int tileIndex = Integer.parseInt(array[indexInArray].trim());
            indexInArray++;
            return tileIndex;
        }
    }


    private static class PCWorldLoader implements Loader {
        Scanner scanner;

        PCWorldLoader(String path) {
            try {
                scanner = new Scanner(new File(path));
            } catch (FileNotFoundException e) {
                System.err.println("PCWorldLoader: File " + path + " not found!");
            }
        }

        @Override
        public int nextInt() {
            return scanner.nextInt();
        }
    }
}
