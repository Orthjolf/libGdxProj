package com.mygdx.game.utils.files;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.ChainShape;

import java.util.ArrayList;

/**
 * Класс-генератор коллизий. Берет двумерный массив тайлов и генерирует
 * на его основе коллекцию чейншейпов.
 *
 * Created by Wiliam on 05.09.2017.
 */

class CollisionGenerator {

    // Константы типов углов
    private static final int noCorner = -1;
    private static final int topLeftInner = -2;
    private static final int topLeftOuter = -3;
    private static final int topRightInner = -4;
    private static final int topRightOuter = -5;
    private static final int bottomLeftInner = -6;
    private static final int bottomLeftOuter = -7;
    private static final int bottomRightInner = -8;
    private static final int bottomRightOuter = -9;

    // Значение, обозначающее то, что эту точку уже прошли
    private static final int poop = -10;

    // Константы направлений
    private static final int noDirection = 0;
    private static final int right = 1;
    private static final int down = 2;
    private static final int left = 3;
    private static final int up = 4;

    // Направление обхода
    private int direction = noDirection;

    // Координаты начала обхода
    private int lineBeginX;
    private int lineBeginY;

    // Текущие координаты обходчика
    private int x = 1;
    private int y = 1;

    // Началась ли обработка фигуры
    private boolean figureStarted;

    // Массив тайловой карты
    private int[][] array;


    /**
     * Конструктор генератора
     *
     * @param array - массив, который нужно обработать
     */
    CollisionGenerator(int[][] array) {
        this.array = array;
    }


    /**
     * Генерирует коллекцию шейпов по двумерному массиву,
     * путем обхода всех фигур по периметру от одного угла к другому.
     * Сперва подготавливаем массив, производя предварительные процедуры с массивом
     * Если точка является углом, вызываем функцию получения фигуры, которой пренадлежит
     * эта точка
     *
     * @return - коллекция шейпов
     */
    ArrayList<ChainShape> generateShapes() {
        ArrayList<ChainShape> chainShapes = new ArrayList<ChainShape>();

        array = expandArray();
        array = zoomArray();
        array = markCornersInArray();

        int width = array[0].length;
        int height = array.length;
        ChainShape shape;
        ArrayList<Vector2> pointsOfChainShape;

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (thisIsCorner(array[y][x])) {
                    setStartPositionOfBypass(y, x);
                    pointsOfChainShape = getPointsOfShape();

                    Vector2[] arrayOfPoints = new Vector2[pointsOfChainShape.size()];
                    pointsOfChainShape.toArray(arrayOfPoints);

                    shape = new ChainShape();
                    shape.createLoop(arrayOfPoints);
                    chainShapes.add(shape);
                }
            }
        }

        return chainShapes;
    }


    /**
     * Устанавливает позицию начала обхода фигуры,
     * Индикатор того, что обход начался, устанавливает в true
     *
     * @param y - y координата
     * @param x - x координата
     */
    private void setStartPositionOfBypass(int y, int x) {
        figureStarted = true;
        lineBeginX = x;
        lineBeginY = y;
        this.x = x;
        this.y = y;
    }


    /**
     * Обходит фигуру по периметру. Если натыкается на угол, добавляет эту точку
     * в коллекцию, перенаправляется в соответствующее направление, и идет по нему,
     * пока не наткнется на другой угол. Цикл заканчивается, если итератор дошел до
     * места, откуда он начал перемещение.
     * Смещение y-1 и x-1 связано с тем, что массив расширен функцией zoomArray(), и координаты
     * сдвинулись на 1.
     *
     * @return - коллекция угловых точек
     */
    private ArrayList<Vector2> getPointsOfShape() {
        ArrayList<Vector2> points = new ArrayList<Vector2>();
        do {
            if (thisIsCorner(array[y][x])) {
                points.add(new Vector2(x - 1, y - 1));
                direction = changeDirection();
            }
            array[y][x] = poop;
            move(direction);

            if (lineBeginX == x && lineBeginY == y) {
                figureStarted = false;
                direction = noDirection;
            }

        } while (figureStarted);
        return points;
    }


    /**
     * Является ли точка углом
     *
     * @param value - значение точки
     *
     * @return - является или нет точка углом
     */
    private boolean thisIsCorner(int value) {
        return value == topLeftInner ||
               value == topLeftOuter ||
               value == topRightInner ||
               value == topRightOuter ||
               value == bottomLeftInner ||
               value == bottomLeftOuter ||
               value == bottomRightInner ||
               value == bottomRightOuter;
    }


    /**
     * Двигает итератор по направлению движения
     *
     * @param direction - направление
     */
    private void move(int direction) {
        switch (direction) {
            case right:
                this.x++;
                break;
            case left:
                this.x--;
                break;
            case up:
                this.y--;
                break;
            case down:
                this.y++;
                break;
        }
    }


    /**
     * Меняет направление движения обхода в зависимости от типа угла
     *
     * @return - новое направление
     */
    private int changeDirection() {

        if (array[y][x] == topLeftInner ||
            array[y][x] == topLeftOuter) {
            if (direction == noDirection || direction == up)
                return right;
            else
                return down;
        }
        if (array[y][x] == topRightInner ||
            array[y][x] == topRightOuter) {
            if (direction == right)
                return down;
            else
                return left;
        }
        if (array[y][x] == bottomRightInner ||
            array[y][x] == bottomRightOuter) {
            if (direction == right)
                return up;
            else
                return left;
        }
        if (array[y][x] == bottomLeftInner ||
            array[y][x] == bottomLeftOuter) {
            if (direction == left)
                return up;
            else
                return right;
        }
        return noCorner;
    }


    /**
     * Расширение массива во все стороны, чтоб избежать indexOutOfBoundsException
     *
     * @return - расширенный массив
     */
    private int[][] expandArray() {
        int width = array[0].length + 2;
        int height = array.length + 2;
        int[][] tempArray = new int[width][height];
        for (int i = 1; i < width - 1; i++)
            System.arraycopy(array[i - 1], 0, tempArray[i], 1, height - 1 - 1);
        return tempArray;
    }


    /**
     * "Увеличение" массива в 2 раза
     *
     * @return - увеличенный массив
     */
    private int[][] zoomArray() {
        int[][] temp = new int[array.length * 2][array[0].length * 2];
        int width = array[0].length;
        int height = array.length;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                temp[y * 2][x * 2] = array[y][x];
                temp[y * 2 + 1][x * 2] = array[y][x];
                temp[y * 2][x * 2 + 1] = array[y][x];
                temp[y * 2 + 1][x * 2 + 1] = array[y][x];
            }
        }
        return temp;
    }


    /**
     * Помечает все углы в массиве разными значениями, для того, чтобы при обходе
     * менять направление движения в зависимости от типа угла
     *
     * @return - массив с воткнутыми углами
     */
    private int[][] markCornersInArray() {
        int[][] arrayWithAngles = array.clone();
        int width = array[0].length;
        int height = array.length;

        int cornerType;
        for (int y = 1; y < height - 1; y++) {
            for (int x = 1; x < width - 1; x++) {
                cornerType = getCornerType(x, y);
                if (cornerType != noCorner)
                    arrayWithAngles[x][y] = cornerType;
            }
        }

        return arrayWithAngles;
    }


    /**
     * Функция узнает какой тип угла в данной точке массива
     *
     * @param y - коодината х
     * @param x - координата y
     *
     * @return - тип угла
     */
    private int getCornerType(int y, int x) {
        // Левый верхний внешний
        if (array[y][x] == 0 &&
            array[y][x + 1] == 0 &&
            array[y + 1][x] == 0 &&
            array[y + 1][x + 1] != 0) {
            return topLeftOuter;
        }
        // Левый верхний внутренний
        else if (array[y][x] != 0 &&
                 array[y][x + 1] != 0 &&
                 array[y + 1][x] != 0 &&
                 array[y + 1][x + 1] == 0) {
            return topLeftInner;
        }
        // Правый верхний внешний
        else if (array[y][x] == 0 &&
                 array[y][x + 1] == 0 &&
                 array[y + 1][x] != 0 &&
                 array[y + 1][x + 1] == 0) {
            return topRightOuter;
        }
        // Правый верхний внутренний
        else if (array[y][x] != 0 &&
                 array[y][x + 1] != 0 &&
                 array[y + 1][x] == 0 &&
                 array[y + 1][x + 1] != 0) {
            return topRightInner;
        }

        // Левый нижний внешний
        else if (array[y][x] == 0 &&
                 array[y][x + 1] != 0 &&
                 array[y + 1][x] == 0 &&
                 array[y + 1][x + 1] == 0) {
            return bottomLeftOuter;
        }

        // Левый нижний внутренний
        else if (array[y][x] != 0 &&
                 array[y][x + 1] == 0 &&
                 array[y + 1][x] != 0 &&
                 array[y + 1][x + 1] != 0) {
            return bottomLeftInner;
        }

        // Правый нижний внешний
        else if (array[y][x] != 0 &&
                 array[y][x + 1] == 0 &&
                 array[y + 1][x] == 0 &&
                 array[y + 1][x + 1] == 0) {
            return bottomRightOuter;
        }

        // Правый нижний внутренний
        else if (array[y][x] == 0 &&
                 array[y][x + 1] != 0 &&
                 array[y + 1][x] != 0 &&
                 array[y + 1][x + 1] != 0) {
            return bottomRightInner;
        }
        else
            return noCorner;
    }


    /**
     * Вывод массива в удобочитаемой форме
     *
     * @param temp - массив
     */
    private void printArray(int[][] temp) {
        for (int[] aTemp : temp) {
            for (int x = 0; x < temp[0].length; x++) {
                switch (aTemp[x]) {
                    case 0:
                        System.out.print(" ");
                        break;
                    case 1:
                        System.out.print("#");
                        break;
                    case -10:
                        System.out.print("*");
                        break;
                    default:
                        System.out.print(-aTemp[x]);
                }
                System.out.print(" ");
            }
            System.out.println();
        }
    }
}
