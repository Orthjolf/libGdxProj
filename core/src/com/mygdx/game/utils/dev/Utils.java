package com.mygdx.game.utils.dev;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 * Created by Wiliam on 13.08.2017.
 */

public class Utils {

    private static File tagsFolder = new File("../../.git/refs/tags");
    private static File coreFolder = new File("../../core/src/com/mygdx/game");

    private static int countOfLines = 0;
    private static long startTime;

    // getLineCountInFile
    //===========================

    /**
     * @return Вовращает последнюю версию проекта с гитлаба
     */

    public static String getLastVersion() {
        File[] versions = tagsFolder.listFiles();
        long latestDate = 0;
        String latestTag = "";

        if (versions == null)
            return "";

        for (File version : versions)
            if (version.lastModified() > latestDate) {
                latestDate = version.lastModified();
                latestTag = version.getName();
            }

        return latestTag;
    }

    /**
     * @return Возвращает количество строк в проекте
     */
    public static int getLineCountInFile() {
        return getLineCountInFile(coreFolder);
    }

    private static int getLineCountInFile(File file) {
        try {
            File[] filesInCoreFolder = file.listFiles();
            if (filesInCoreFolder != null)
                for (File f : filesInCoreFolder)
                    if (f.isDirectory())
                        getLineCountInFile(f);
                    else {
                        BufferedReader br = new BufferedReader(new FileReader(f));
                        while (br.readLine() != null) {
                            //     System.out.println(countOfLines + " : " + br.readLine());
                            countOfLines++;
                        }
                    }
        } catch (Exception e) {
            System.out.println("A?");
        }
        return countOfLines;
    }

    public static void startTimer() {
        startTime = System.nanoTime();
    }

    public static double stopTimer() {
        long finishTime = System.nanoTime();
        long elapsedTime = finishTime - startTime;
        return (double) elapsedTime / 1000000000.0;
    }
}
