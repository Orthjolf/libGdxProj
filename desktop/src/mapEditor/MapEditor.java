package mapEditor;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.mygdx.game.Screens.ScreenTransition;

import mapEditor.Screens.LoadingScreen;
import mapEditor.Screens.EditorScreen;

/**
 * Главный класс редактора уровней
 *
 * @author Wilhelm Novitsky 01.06.2017
 */
public class MapEditor extends Game {

	private static final String TITLE = "MapEditor";

	private static MapEditor instance;
	public AssetManager assets;
	public SpriteBatch batch;
	public OrthographicCamera camera;
	public mapEditor.Screens.EditorScreen editorScreen;
	public LoadingScreen loadingScreen;
	public BitmapFont font24;
	private ScreenTransition screenTransition;

	/**
	 * Создает одиночный экземпляр класса, если такого не существует, и возвращает его.
	 */
	public static MapEditor get() {
		if (instance == null)
			instance = new MapEditor();
		return instance;
	}


	@Override
	public void create() {
		camera = new OrthographicCamera();
		Gdx.graphics.setTitle(TITLE);
		assets = new AssetManager();
		batch = new SpriteBatch();
		initFonts();

		screenTransition = new ScreenTransition(this);
		editorScreen = new EditorScreen();
		loadingScreen = new LoadingScreen();

		callScreen(loadingScreen);
	}


	@Override
	public void render() {
		Gdx.gl20.glClearColor(0.3f, 0.3f, 0.3f, 1f);
		Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);

		super.render();

		screenTransition.update();
		screenTransition.draw();

		if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
			Gdx.app.exit();
		}
	}


	/**
	 * Инициализация шрифтов
	 */
	private void initFonts() {
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("ui/Arcon.ttf"));
		FreeTypeFontGenerator.FreeTypeFontParameter params = new FreeTypeFontGenerator.FreeTypeFontParameter();

		params.size = 24;
		params.color = Color.BLACK;
		font24 = generator.generateFont(params);
	}


	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		screenTransition.resize(width, height);
	}


	@Override
	public void dispose() {
		batch.dispose();
		assets.dispose();
	}


	/**
	 * Вызов нового экрана
	 */
	public void callScreen(Screen nextScreen) {
		screenTransition.init(nextScreen);
	}
}