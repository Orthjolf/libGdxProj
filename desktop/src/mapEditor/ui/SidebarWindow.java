package mapEditor.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.mygdx.game.world.map.objects.MapObject;

import mapEditor.ui.Components.FormPane;
import mapEditor.ui.Components.TabPane;
import mapEditor.ui.Components.TabPaneButton;

/**
 * Сайдбар для редактора уровней
 *
 * Created by Wiliam on 19.10.2017.
 */

class SidebarWindow extends Table {
	private static final float WIDTH = 352f;
	private TabPane pne_tabs;
	private FileMenu mnu_fileMenu;
	/*private MapMenu mnu_mapMenu;
	private ObjectMenu mnu_objectMenu;
	private SystemMenu mnu_systemMenu;*/


	SidebarWindow(Skin skin) {
		mnu_fileMenu = new FileMenu();
		/*mnu_mapMenu = new MapMenu();
		mnu_objectMenu = new ObjectMenu();
		mnu_systemMenu = new SystemMenu();*/

		pne_tabs = new TabPane();
		pne_tabs.addTab(new TabPaneButton(Ui.editor_toolbar_file, skin, "toggle"), mnu_fileMenu);
//		pne_tabs.addTab(new TabPaneButton(Ui.editor_toolbar_map, skin, "toggle"), mnu_mapMenu);
//		pne_tabs.addTab(new TabPaneButton(Ui.editor_toolbar_objects, skin, "toggle"), mnu_objectMenu);
//		pne_tabs.addTab(new TabPaneButton(Ui.editor_toolbar_system, skin, "toggle"), mnu_systemMenu);

		this.add(new FormPane(pne_tabs, skin, "dark")).expand().fill();
		this.pack();
		this.setVisible(true);
	}


	public void update() {
//		mnu_mapMenu.updateComponents();
//		mnu_objectMenu.updateComponents();
	}


	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);

		batch.setColor(Color.WHITE);

		Ui.skin_shadow.draw(batch, this.getX() + this.getWidth(),
		                    this.getY(),
		                    Ui.skin_shadow.getRegion().getRegionWidth(),
		                    this.getHeight());
	}


	public int getEditMode() {
//		if (pne_tabs.getVisibleContent() == mnu_mapMenu) {
//			return (mnu_mapMenu.getEditMode());
//		}
//		if (pne_tabs.getVisibleContent() == mnu_objectMenu) {
//			return (mnu_objectMenu.getEditMode());
//		}
//		return (EditorStage.EDIT_NONE);
		return 0;
	}


	public int getSelectedMapTile() {
//		return (mnu_mapMenu.getSelectedMapTile());
		return 0;
	}


	public MapObject getSelectedMapObject() {
//		return (mnu_objectMenu.getSelectedMapObject());
		return null;
	}


	void resize(int width, int height) {
		this.setSize(WIDTH, height);
	}
}