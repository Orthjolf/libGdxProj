package mapEditor.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable;

import mapEditor.MapEditor;
import mapEditor.Screens.EditorScreen;

import static mapEditor.Screens.EditorScreen.height;

/**
 * Юзер интерфейс для редактора
 *
 * Created by Wiliam on 16.10.2017.
 */

public class Ui {

	public static int sideBarWidth = 300;
	public static Skin skin;
	public static TiledDrawable skin_shadow;

	public static Sprite editor_icon_scrollCenter;
	public static Sprite editor_icon_markObject;
	public static Sprite editor_icon_leftMouse;
	public static Sprite editor_icon_rightMouse;

	public static Sprite editor_toolbar_file;
	public static Sprite editor_toolbar_map;
	public static Sprite editor_toolbar_objects;
	public static Sprite editor_toolbar_system;
	public static Sprite editor_toolbar_file_new;
	public static Sprite editor_toolbar_file_save;
	public static Sprite editor_toolbar_file_load;
	public static Sprite editor_toolbar_map_environment;
	public static Sprite editor_toolbar_map_tiles;
	public static Sprite editor_toolbar_map_conditions;
	public static Sprite editor_toolbar_obj_place;
	public static Sprite editor_toolbar_obj_attributes;
	public static Sprite editor_toolbar_sys_settings;
	public static Sprite editor_toolbar_sys_menu;
	public TextureAtlas atlas_ui;
	public BitmapFont font_small;
	public BitmapFont font_medium;
	public BitmapFont font_big;
	// Editor sprites: Rectangles
	//===========================
	public Sprite editor_rect_green;
	public Sprite editor_rect_red;
	public Sprite editor_rect_white;
	public Sprite editor_rect_yellow;
	TextField textField;
	private SidebarWindow sidebarWindow;
	private Label lbl_info;
	private StringBuilder infoText;
	private Stage stage;
	private EditorScreen screen;
	private TextButton
			tiledMapEditorMode,
			sideBar,
			fileManagementMode,
			objectiveMapEditorMode,
			objectEditorMode;
	private MapEditor app;

	public Ui(final EditorScreen screen) {
		this.screen = screen;
		stage = screen.stage;
		app = MapEditor.get();

		atlas_ui = new TextureAtlas(Gdx.files.internal("resources/images/ui/ui.atlas"));
		initFonts();
		initSkin();
		initEditorUi();
		initButtons();

		lbl_info = new Label("", skin, "popup");
		infoText = new StringBuilder("");
		sidebarWindow = new SidebarWindow(skin);

		stage.addActor(sidebarWindow);
	}


	private void initFonts() {
		FileHandle fontFile = Gdx.files.internal("resources/fonts/font.ttf");
		FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(fontFile);
		{
			parameter.size = 16;
			font_small = generator.generateFont(parameter);
			parameter.size = 24;
			font_medium = generator.generateFont(parameter);
			parameter.size = 32;
			font_big = generator.generateFont(parameter);
		}
		generator.dispose();

		font_small.setUseIntegerPositions(true);
		font_medium.setUseIntegerPositions(true);
		font_big.setUseIntegerPositions(true);
	}


	private void initSkin() {
		skin = new Skin();
		skin.addRegions(atlas_ui);

		skin.add("font_small", font_small);
		skin.add("font_medium", font_medium);
		skin.add("font_big", font_big);
		skin.load(Gdx.files.internal("resources/images/ui/ui.json"));

		skin_shadow = new TiledDrawable(atlas_ui.createSprite("sidebar_shadow"));
	}

	private void initEditorUi() {
		// Color rectangles
		editor_rect_green = atlas_ui.createSprite("rect_green");
		editor_rect_red = atlas_ui.createSprite("rect_red");
		editor_rect_white = atlas_ui.createSprite("rect_white");
		editor_rect_yellow = atlas_ui.createSprite("rect_yellow");

		// Icons
		editor_icon_scrollCenter = atlas_ui.createSprite("icon_scrollCenter");
		editor_icon_markObject = atlas_ui.createSprite("icon_markObject");
		editor_icon_leftMouse = atlas_ui.createSprite("icon_leftMouse");
		editor_icon_rightMouse = atlas_ui.createSprite("icon_rightMouse");

		// Toolbar: main
		editor_toolbar_file = atlas_ui.createSprite("toolbar_file");
		editor_toolbar_map = atlas_ui.createSprite("toolbar_map");
		editor_toolbar_objects = atlas_ui.createSprite("toolbar_objects");
		editor_toolbar_system = atlas_ui.createSprite("toolbar_system");

		// Toolbar: file
		editor_toolbar_file_new = atlas_ui.createSprite("toolbar_file_new");
		editor_toolbar_file_save = atlas_ui.createSprite("toolbar_file_save");
		editor_toolbar_file_load = atlas_ui.createSprite("toolbar_file_load");

		// Toolbar: world
		editor_toolbar_map_environment = atlas_ui.createSprite("toolbar_map_environment");
		editor_toolbar_map_tiles = atlas_ui.createSprite("toolbar_map_tiles");
		editor_toolbar_map_conditions = atlas_ui.createSprite("toolbar_map_conditions");

		// Toolbar: object
		editor_toolbar_obj_place = atlas_ui.createSprite("toolbar_obj_place");
		editor_toolbar_obj_attributes = atlas_ui.createSprite("toolbar_obj_attributes");

		// Toolbar: system
		editor_toolbar_sys_menu = atlas_ui.createSprite("toolbar_sys_menu");
		editor_toolbar_sys_settings = atlas_ui.createSprite("toolbar_sys_settings");
	}

	private void initButtons() {
		sideBar = new TextButton("", skin, "default");
		sideBar.setPosition(0, 0);
		sideBar.setSize(sideBarWidth, height);
		sideBar.setDisabled(true);
		stage.addActor(sideBar);

		tiledMapEditorMode = new TextButton("Tiles", skin, "default");
		tiledMapEditorMode.setPosition(0, height - 90);
		tiledMapEditorMode.setSize(60, 60);
		tiledMapEditorMode.addListener(createListener(1));
		stage.addActor(tiledMapEditorMode);

		objectiveMapEditorMode = new TextButton("Objs", skin, "default");
		objectiveMapEditorMode.setPosition(60, height - 90);
		objectiveMapEditorMode.setSize(60, 60);
		objectiveMapEditorMode.addListener(createListener(2));
		stage.addActor(objectiveMapEditorMode);

		fileManagementMode = new TextButton("Files", skin, "default");
		fileManagementMode.setPosition(120, height - 90);
		fileManagementMode.setSize(60, 60);
		stage.addActor(fileManagementMode);

		objectEditorMode = new TextButton("Obj", skin, "default");
		objectEditorMode.setPosition(180, height - 90);
		objectEditorMode.setSize(60, 60);
		stage.addActor(objectEditorMode);

//		textField = new TextField("", skin);
//		textField.setPosition(100,100);
//		textField.setSize(100,60);
//		stage.addActor(textField);
	}


	/**
	 * Метод создания обработчкика кнопки
	 *
	 * @param numberOfInputController - номер контроллера, который будет устанавливаться
	 *                                в обработчике
	 *
	 * @return - обработчик, меняющий контроллер при срабатывании
	 */
	private ClickListener createListener(final int numberOfInputController) {
		return new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				screen.editorInput.setInputController(numberOfInputController);
			}
		};
	}

	public void draw() {
		stage.draw();
	}

	public void update() {
		sidebarWindow.update();
	}

	public void resize(int width, int height) {
		sidebarWindow.resize(width, height);
	}
}
