package mapEditor.ui;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.io.File;

import mapEditor.ui.Components.FormButton;
import mapEditor.ui.Components.FormList;
import mapEditor.ui.Components.FormPane;
import mapEditor.ui.Components.SidebarScrollPane;
import mapEditor.ui.Components.SidebarTable;
import mapEditor.ui.Components.TabPane;
import mapEditor.ui.Components.TabPaneButton;

/**
 * Меню для работы с файлами
 *
 * Created by Wiliam on 19.10.2017.
 */

class FileMenu extends Table {
	private Skin skin;

	private TabPane pne_tabs;
	private Actor mnu_createWorld;
	private Actor mnu_saveWorld;
	private Actor mnu_loadWorld;

	private TextField txt_fileName;
	private FormList<String> lst_worldFiles_load;
	private FormList<String> lst_worldFiles_save;
	private String[] worldPaths;
	private String[] worldNames;


	public FileMenu() {
		skin = Ui.skin;

		mnu_createWorld = new SidebarScrollPane(this.createCreateMenu(), skin, "transparent");
		mnu_saveWorld = new SidebarScrollPane(this.createSaveMenu(), skin, "transparent");
		mnu_loadWorld = new SidebarScrollPane(this.createLoadMenu(), skin, "transparent");

		pne_tabs = new TabPane();

		pne_tabs.addTab(new TabPaneButton(Ui.editor_toolbar_file_new, skin, "toggle"), mnu_createWorld);
		pne_tabs.addTab(new TabPaneButton(Ui.editor_toolbar_file_save, skin, "toggle"), mnu_saveWorld);
		pne_tabs.addTab(new TabPaneButton(Ui.editor_toolbar_file_load, skin, "toggle"), mnu_loadWorld);

		pne_tabs.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Actor content = pne_tabs.getVisibleContent();

				if (content == mnu_saveWorld)
					updateWorldFilesList();
				if (content == mnu_loadWorld)
					updateWorldFilesList();
			}
		});

		this.add(new FormPane(pne_tabs, skin)).top().expand().fill();

		this.updateWorldFilesList();
//		txt_fileName.setText(lst_worldFiles_save.getSelected());
	}


	private Actor createCreateMenu() {
		final TextField txt_title = new TextField("editor_newWorld", skin);
		final TextField txt_width = new TextField("10", skin);
		final TextField txt_height = new TextField("10", skin);

		txt_width.setTextFieldFilter(new TextField.TextFieldFilter.DigitsOnlyFilter());
		txt_height.setTextFieldFilter(new TextField.TextFieldFilter.DigitsOnlyFilter());

		txt_title.setMaxLength(30);
		txt_width.setMaxLength(3);
		txt_height.setMaxLength(3);

		FormButton btn_create = new FormButton("editor_file_create", skin);
		btn_create.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent e, Actor actor) {
				if (Integer.parseInt(txt_width.getText()) < 4)
					txt_width.setText("4");
				if (Integer.parseInt(txt_height.getText()) < 4)
					txt_height.setText("4");
				if (Integer.parseInt(txt_width.getText()) > 500)
					txt_width.setText("500");
				if (Integer.parseInt(txt_height.getText()) > 500)
					txt_height.setText("500");
			}
		});

		Table tbl_main = new SidebarTable();

		tbl_main.columnDefaults(0).width(Value.percentWidth(0.00f, tbl_main));
		tbl_main.columnDefaults(1).width(Value.percentWidth(0.40f, tbl_main));

		tbl_main.add(new Label("editor_file_createWorld", skin, "heading")).colspan(2).left();
		tbl_main.row();

		tbl_main.add(new Label("editor_file_worldName", skin)).left();
		tbl_main.add(txt_title).right();
		tbl_main.row();

		tbl_main.add(new Label("editor_file_width", skin)).left();
		tbl_main.add(txt_width).right();
		tbl_main.row();

		tbl_main.add(new Label("editor_file_height", skin)).left();
		tbl_main.add(txt_height).right();
		tbl_main.row();

		tbl_main.add();
		tbl_main.add(btn_create).right().width(btn_create.getPrefWidth());

		return tbl_main;
	}


	private Actor createSaveMenu() {
		txt_fileName = new TextField("", skin);

		lst_worldFiles_save = new FormList<String>(skin);
		lst_worldFiles_save.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent e, float x, float y) {
				if (lst_worldFiles_save.getSelected() != null) {
					txt_fileName.setText(lst_worldFiles_save.getSelected());
				}
			}
		});


		FormButton btn_save = new FormButton("editor_file_save", skin);
		btn_save.addListener(new ChangeListener() {

			@Override
			public void changed(ChangeEvent e, Actor actor) {
				if (txt_fileName.getText().length() > 0) {
					//WorldSaver.saveWorld()
					System.out.println("Файл сохранен");
				}
			}
		});

		Table tbl_main = new SidebarTable();

		tbl_main.add(new Label("editor_file_saveWorld", skin, "heading")).colspan(2).left();
		tbl_main.row();

		SidebarScrollPane filesPane = new SidebarScrollPane(lst_worldFiles_save, skin, "dark");

		tbl_main.add(filesPane).top().expand().fill().colspan(2);
		tbl_main.row();

		tbl_main.add(txt_fileName).expandX().fill();
		tbl_main.add(btn_save).expand(false, false);

		return (tbl_main);
	}


	private Actor createLoadMenu() {
		lst_worldFiles_load = new FormList<String>(skin);
		FormButton btn_load = new FormButton("editor_file_load", skin);
		btn_load.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent e, Actor actor) {
				if (lst_worldFiles_load.getSelected() != null) {
					// TODO загрузка уровня
				}
//				getStage().update();
			}
		});

		Table tbl_main = new SidebarTable();

		tbl_main.add(new Label("editor_file_loadWorld", skin, "heading")).left();
		tbl_main.row();

		SidebarScrollPane pne_filesList = new SidebarScrollPane(lst_worldFiles_load, skin, "dark");

		tbl_main.add(pne_filesList).top().expand().fill();
		tbl_main.row();
		tbl_main.add(btn_load).right();

		return (tbl_main);
	}


	private void updateWorldFilesList() {

		File[] files = new File("resources\\worlds").listFiles();

		worldNames = new String[files.length];
		worldPaths = new String[files.length];

		for (int i = 0; i < files.length; i++) {
			worldNames[i] = files[i].getName();
			worldPaths[i] = files[i].getAbsolutePath();
		}

		int idx_load = lst_worldFiles_load.getSelectedIndex();
		int idx_save = lst_worldFiles_save.getSelectedIndex();

		if (idx_load < 0)
			idx_load = 0;
		if (idx_save < 0)
			idx_save = 0;

		lst_worldFiles_load.clearItems();
		lst_worldFiles_load.setItems(worldNames);
		lst_worldFiles_load.setSelectedIndex(lst_worldFiles_load.getItems().size - 1);

		if (lst_worldFiles_load.getItems().size > idx_load) {
			lst_worldFiles_load.setSelectedIndex(idx_load);
		}

		lst_worldFiles_save.clearItems();
		lst_worldFiles_save.setItems(worldNames);
		lst_worldFiles_save.setSelectedIndex(lst_worldFiles_save.getItems().size - 1);

		if (lst_worldFiles_save.getItems().size > idx_save) {
			lst_worldFiles_save.setSelectedIndex(idx_save);
		}
	}

	@Override
	public void setVisible(boolean value) {
		super.setVisible(value);
		this.updateWorldFilesList();
	}
}
