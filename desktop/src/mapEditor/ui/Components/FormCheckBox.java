package mapEditor.ui.Components;

import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Align;

/**
 * Чек-бокс для редактора
 *
 * Created by Wiliam on 19.10.2017.
 */

public class FormCheckBox extends CheckBox {
	public FormCheckBox(String text, Skin skin) {
		super(text, skin);
		this.align(Align.right);
	}
}