package mapEditor.ui.Components;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * Панель вкладок
 *
 * Created by Wiliam on 19.10.2017.
 */

public class TabPane extends Table {

	private ButtonGroup<Button> buttonGroup;
	private Stack contentStack;
	private Table tbl_tabs;


	public TabPane() {
		buttonGroup = new ButtonGroup<Button>();
		contentStack = new Stack();
		tbl_tabs = new Table();
	}


	public void addTab(final Button button, Actor content) {
		button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent e, float x, float y) {
				setActiveTab(button);
				TabPane.this.fire(new ChangeListener.ChangeEvent());
			}
		});
		button.pad(0);


		buttonGroup.add(button);
		contentStack.add(content);
		tbl_tabs.add(button).prefWidth(999)
		        .height(55)
		        .pad(2);

		this.clear();
		this.add(tbl_tabs).padBottom(8).left().row();
		this.add(contentStack).pad(0f, 2, 2, 2).expand().fill().width(0f);
		this.setActiveTab(0);
	}


	public void removeTab(Actor content) {
		for (int i = 0; i < contentStack.getChildren().size; i++) {
			if (contentStack.getChildren().get(i).equals(content)) {
				Button btn = buttonGroup.getButtons().get(i);
				btn.remove();
				buttonGroup.remove(btn);
				contentStack.getChildren().get(i).remove();
				break;
			}
		}
	}


	public void setActiveTab(int index) {
		this.setActiveTab(buttonGroup.getButtons().get(index));
	}


	private void setActiveTab(Button tab) {
		int contentId = 0;

		for (int i = 0; i < buttonGroup.getButtons().size; i++) {
			if (buttonGroup.getButtons().get(i) == tab) {
				buttonGroup.getButtons().get(i).setChecked(true);
				contentId = i;
			}
		}


		for (int i = 0; i < contentStack.getChildren().size; i++) {
			contentStack.getChildren().get(i).setVisible(false);

			if (i == contentId) {
				contentStack.getChildren().get(i).setVisible(true);
			}
		}
	}


	public Actor getVisibleContent() {
		for (Actor actor : contentStack.getChildren()) {
			if (actor.isVisible()) {
				return (actor);
			}
		}
		return null;
	}


	@Override
	public float getPrefWidth() {
		return tbl_tabs.getWidth();
	}
}