package mapEditor.ui.Components;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

/**
 * Панелька прокрутки
 *
 * Created by Wiliam on 19.10.2017.
 */

public class FormPane extends ScrollPane {
	public FormPane(Actor content, Skin skin) {
		super(content, skin);
		this.setScrollingDisabled(true, true);
	}


	public FormPane(Actor content, Skin skin, String styleName) {
		super(content, skin, styleName);
		this.setScrollingDisabled(true, true);
	}
}