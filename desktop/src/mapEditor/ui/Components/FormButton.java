package mapEditor.ui.Components;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

/**
 * Кнопка для редактора
 *
 * Created by Wiliam on 19.10.2017.
 */

public class FormButton extends TextButton {

	public FormButton(String text, Skin skin) {
		this(text, skin, "default");
	}


	public FormButton(String text, Skin skin, String styleName) {
		super(text, skin, styleName);
		this.pad(0);
	}


	@Override
	public float getPrefWidth() {
		float oldWidth = super.getPrefWidth() + 14f;
		float newWidth = 0f;
		float step = 35f;

		for (int i = 1; i < 999; i++) {
			if (oldWidth < step * i) {
				newWidth = step * i;
				break;
			}
		}
		return newWidth;
	}


	@Override
	public float getPrefHeight() {
		return super.getPrefHeight() + 6f;
	}
}