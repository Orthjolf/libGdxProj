package mapEditor.ui.Components;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

/**
 * Панель прокрутки
 *
 * Created by Wiliam on 19.10.2017.
 */

public class SidebarScrollPane extends ScrollPane {
	public SidebarScrollPane(Actor content, Skin skin) {
		this(content, skin, "default");
	}


	public SidebarScrollPane(Actor content, Skin skin, String styleName) {
		super(content, skin, styleName);

		this.setScrollbarsOnTop(true);
		this.setFadeScrollBars(false);
		this.setFlickScroll(false);
		this.setScrollingDisabled(true, false);
	}
}