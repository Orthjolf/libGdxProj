package mapEditor.ui.Components;

import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import mapEditor.ui.Ui;

/**
 * Форма выбора редактора
 *
 * Created by Wiliam on 19.10.2017.
 */

public class FormSelectBox<T> extends SelectBox<T> {
	float height = new FormButton("", Ui.skin).getPrefHeight();

	public FormSelectBox(Skin skin) {
		this(skin, "default");
	}


	public FormSelectBox(Skin skin, String style) {
		super(skin, style);
	}


	@Override
	public float getPrefHeight() {
		return (height);
	}
}