package mapEditor.ui.Components;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * Кнопка на панели со вкладками
 *
 * Created by Wiliam on 19.10.2017.
 */

public class TabPaneButton extends ImageButton {
	public TabPaneButton(Sprite sprite, Skin skin, String styleName) {
		super(skin, "tabpane");
		this.add(new Image(new TextureRegionDrawable(sprite)));
	}
}
