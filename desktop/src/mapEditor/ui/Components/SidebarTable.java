package mapEditor.ui.Components;

import com.badlogic.gdx.scenes.scene2d.ui.Table;

/**
 * Таблица для сайдбара
 *
 * Created by Wiliam on 19.10.2017.
 */

public class SidebarTable extends Table {
	public SidebarTable() {
		super();
		this.top()
		    .left()
		    .pad(8)
		    .padTop(0f);
		this.defaults()
		    .space(8)
		    .expandX()
		    .fillX();
	}
}
