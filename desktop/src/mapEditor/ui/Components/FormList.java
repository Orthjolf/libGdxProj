package mapEditor.ui.Components;

import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import java.util.ArrayList;

/**
 * Форма списка
 *
 * Created by Wiliam on 19.10.2017.
 */

public class FormList<T> extends List<T> {
	public FormList(Skin skin) {
		this(skin, "default");
	}


	public FormList(Skin skin, String style) {
		super(skin, style);
	}


	public void setItems(ArrayList<T> items) {
		int selected = this.getSelectedIndex();
		this.clearItems();
		for (T item : items) {
			this.addItem(item);
		}

		this.setSelectedIndex(selected);
	}


	public void addItem(T item) {
		this.getItems().add(item);
		this.setSelectedIndex(this.getItems().size - 1);
	}


	public void removeItem() {
		// Removes selected item
		//======================
		if (this.getItems().size > 0) {
			this.removeItem(this.getSelectedIndex());
		}
	}


	public void removeItem(int index) {
		if (index >= 0 && index < this.getItems().size) {
			this.getItems().removeIndex(index);
		}

		int size = this.getItems().size;

		if (index >= size) {
			this.setSelectedIndex(size - 1);
		}
		else if (size != 0) {
			this.setSelectedIndex(index);
		}
	}


	@Override
	public void setSelectedIndex(int index) {
		if (index > this.getItems().size - 1) {
			index = this.getItems().size - 1;
		}
		else if (index < -1) {
			index = -1;
		}
		super.setSelectedIndex(index);
	}
}