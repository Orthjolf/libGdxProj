package mapEditor;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.mygdx.game.resources.ResourceRepository;
import com.mygdx.game.resources.WorldTextureSet;

import static mapEditor.Input.TiledMapInputController.dragPositionStart;
import static mapEditor.Input.TiledMapInputController.mousePosTiles;
import static mapEditor.Input.TiledMapInputController.tileSize;


/**
 * Отрисовщик редактора
 *
 * Created by Wiliam on 17.09.2017.
 */

public class Renderer {
	private int[][] tiledMap;
	private BitmapFont font;
	private mapEditor.Screens.EditorScreen screen;
	private ShapeRenderer shapeRenderer;


	public Renderer(mapEditor.Screens.EditorScreen screen) {
		this.tiledMap = screen.tiledMap;
		this.screen = screen;
		font = new BitmapFont();

		WorldTextureSet.initializeTextures();
	}


	public void render(Batch batch) {
		ResourceRepository.WORLD.dev_grey_background.setPosition(0, 0);
		ResourceRepository.WORLD.dev_grey_background.draw(batch);

		for (int y = 0; y < 20; y++) {
			for (int x = 0; x < 20; x++) {
				if (tiledMap[y][x] == 1) {
					ResourceRepository.WORLD.dev_grey_rect.setPosition(x * tileSize, y * tileSize);
					ResourceRepository.WORLD.dev_grey_rect.draw(batch);
				}
			}
		}

		ResourceRepository.WORLD.dev_grey_rect.setPosition(screen.camera.position.x, screen.camera.position.y);
		ResourceRepository.WORLD.dev_grey_rect.draw(batch);

		drawCursor(batch);
		drawDragArea(batch);
//		shapeRenderer = new ShapeRenderer();
//		shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
//		{
//			shapeRenderer.setColor(0, 1, 0, .5f);
//			shapeRenderer.rect(screen.dragArea.x, screen.dragArea.y, 200, 200);
//			shapeRenderer.setColor(1, 0, 1, 1);
//			shapeRenderer.circle(60, 60, 34);
//		}
//		shapeRenderer.end();
	}

	private void drawCursor(Batch batch) {
		int x = (int) mousePosTiles.x * tileSize;
		int y = (int) mousePosTiles.y * tileSize;
		ResourceRepository.WORLD.dev_grey_rect.setPosition(x, y);
		ResourceRepository.WORLD.dev_grey_rect.setAlpha(0.5f);
		ResourceRepository.WORLD.dev_grey_rect.draw(batch);
		ResourceRepository.WORLD.dev_grey_rect.setAlpha(1);
		font.draw(batch, (int) mousePosTiles.x + " " + (int) mousePosTiles.y, x, y);
	}


	private void drawDragArea(Batch batch) {
		ResourceRepository.WORLD.dev_grey_rect.setAlpha(0.5f);
		int width = (int) (mousePosTiles.x - dragPositionStart.x);
		for (int i = (int) dragPositionStart.x; i <= dragPositionStart.x + width; i++) {
			ResourceRepository.WORLD.dev_grey_rect.setPosition(i * tileSize, (int) mousePosTiles.y * tileSize);
			ResourceRepository.WORLD.dev_grey_rect.draw(batch);
		}
		ResourceRepository.WORLD.dev_grey_rect.setAlpha(1);
	}
}
