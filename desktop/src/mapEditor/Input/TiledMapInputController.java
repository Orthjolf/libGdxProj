package mapEditor.Input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;

import mapEditor.Screens.EditorScreen;
import mapEditor.WorldSaver;

/**
 *
 * Created by Wiliam on 08.10.2017.
 */

public class TiledMapInputController extends InputController {
	public static Vector2 mousePosTiles;
	public static Vector2 dragPositionStart;
	public static Vector2 dragPositionFinish;
	private Vector2 mousePosWorld;
	private boolean isDragged = false;
	private boolean ctrlPressed = false;

	static int selectedTileId = 1;

	public static int tileSize = 64;

	TiledMapInputController(EditorScreen screen, EditorInput global) {
		super(screen,global);
		dragPositionStart = new Vector2();
		dragPositionFinish = new Vector2();
		mousePosWorld = new Vector2();
		mousePosTiles = new Vector2();
	}


	public void update() {
		//updateDragArea();
	}


	@Override
	public boolean keyDown(int key) {
		if (key == Input.Keys.CONTROL_LEFT)
			ctrlPressed = true;

		if (ctrlPressed && key == Input.Keys.S)
			WorldSaver.saveWorld(screen.tiledMap);
		return false;
	}


	@Override
	public boolean keyUp(int key) {
		if (key == Input.Keys.CONTROL_LEFT) {
			ctrlPressed = false;
			System.out.println("!ctrl");
		}

		return false;
	}


	@Override
	public boolean scrolled(int scroll) {
		return true;
	}


	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {
		setGlobalCursorPosition(x, y);
		dragPositionStart.set(mousePosTiles.x, mousePosTiles.y);
		isDragged = true;

		return true;
	}


	@Override
	public boolean touchUp(int x, int y, int pointer, int button) {
		setGlobalCursorPosition(x, y);
		dragPositionFinish.set(mousePosTiles);
		if (button == 0)
			screen.tiledMap[(int) mousePosTiles.y][(int) mousePosTiles.x] = 1;
			//setTilesOnArea();
		else
			screen.tiledMap[(int) mousePosTiles.y][(int) mousePosTiles.x] = 0;
		isDragged = false;
		return false;
	}


	/**
	 * Выставление тайлов в массив
	 */
	private void setTilesOnArea() {
		int width = (int) (dragPositionFinish.x - dragPositionStart.x);
		for (int i = (int) dragPositionStart.x; i <= dragPositionStart.x + width; i++)
			screen.tiledMap[(int) mousePosTiles.y][i] = 1;
		screen.tiledMap[(int) mousePosTiles.y][(int) mousePosTiles.x] = selectedTileId;
	}


	@Override
	public boolean mouseMoved(int x, int y) {
		setGlobalCursorPosition(x, y);
		return false;
	}


	/**
	 * Установка глобальной позиции курсора в мире, и проверка на
	 * выход за границы карты
	 *
	 * @param x - координата x
	 * @param y - координата y
	 */
	private void setGlobalCursorPosition(int x, int y) {
		mousePosWorld.set(
				screen.camera.position.x - screen.width / 2 + x,
				screen.height - y + screen.camera.position.y - screen.height / 2
		);

		int xx = (int) (mousePosWorld.x / tileSize);
		int yy = (int) (mousePosWorld.y / tileSize);
		mousePosTiles.set(xx, yy);

		if (mousePosTiles.x <= 0)
			mousePosTiles.x = 0;
		if (mousePosTiles.y <= 0)
			mousePosTiles.y = 0;
		if (mousePosTiles.x >= screen.tiledMap[0].length - 1)
			mousePosTiles.x = screen.tiledMap[0].length - 1;
		if (mousePosTiles.y >= screen.tiledMap.length - 1)
			mousePosTiles.y = screen.tiledMap.length - 1;
	}


	private void updateDragArea() {
		if(Gdx.input.isTouched()){
		//	System.out.println(1);
		}
		if (!isDragged)
			return;
	}
}
