package mapEditor.Input;

import mapEditor.Screens.EditorScreen;

/**
 * Created by Wiliam on 08.10.2017.
 */

abstract class InputController {

	protected EditorScreen screen;
	protected EditorInput global;

	InputController(EditorScreen screen, EditorInput global) {
		this.screen = screen;
		this.global = global;
	}

	public abstract void update();

	public abstract boolean keyDown(int key);

	public abstract boolean keyUp(int key);

	public abstract boolean scrolled(int scroll);

	public abstract boolean touchDown(int x, int y, int pointer, int button);

	public abstract boolean touchUp(int x, int y, int pointer, int button);

	public abstract boolean mouseMoved(int x, int y);
}
