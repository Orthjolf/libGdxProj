package mapEditor.Input;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Vector2;

import mapEditor.Screens.EditorScreen;

import static mapEditor.ui.Ui.sideBarWidth;


public class EditorInput extends InputAdapter {

	private final int cameraMoveTrashHold = 64;
	private final int cameraSpeed = 5;
	private InputController inputController;
	private Vector2 mousePosScreen;
	private EditorScreen screen;


	public EditorInput(EditorScreen screen) {
		this.screen = screen;
		setInputController(1);
		mousePosScreen = new Vector2(500, 500);
	}


	public void update() {
		inputController.update();
		updateCamera();
	}


	/**
	 * Движение камеры
	 */
	private void updateCamera() {
		if (mousePosScreen.x < cameraMoveTrashHold + sideBarWidth &&
		    mousePosScreen.x > sideBarWidth && screen.camera.position.x > 0)
			screen.camera.position.x -= cameraSpeed;
		if (mousePosScreen.x > EditorScreen.width - cameraMoveTrashHold)
			screen.camera.position.x += cameraSpeed;
		if (mousePosScreen.y > EditorScreen.height - cameraMoveTrashHold &&
		    screen.camera.position.y > 0)
			screen.camera.position.y -= cameraSpeed;
		if (mousePosScreen.y < cameraMoveTrashHold && mousePosScreen.x > sideBarWidth)
			screen.camera.position.y += cameraSpeed;
	}


	@Override
	public boolean mouseMoved(int x, int y) {
		mousePosScreen.set(x, y);
		return inputController.mouseMoved(x, y);
	}


	public void setInputController(int number) {
		switch (number) {
			case 1:
				inputController = new TiledMapInputController(screen, this);
				break;
			case 2:
				inputController = new ObjectiveMapInputController(screen, this);
				break;
		}
	}


	@Override
	public boolean keyDown(int key) {
		// 7 - код клавиши 0
		int controllerNumber = key - 7;
		setInputController(controllerNumber);
		return inputController.keyDown(key);
	}


	@Override
	public boolean keyUp(int key) {
		return inputController.keyUp(key);
	}


	@Override
	public boolean scrolled(int scroll) {
		return inputController.scrolled(scroll);
	}


	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {
		return inputController.touchDown(x, y, pointer, button);
	}


	@Override
	public boolean touchUp(int x, int y, int pointer, int button) {
		return inputController.touchUp(x, y, pointer, button);
	}
}

