package mapEditor;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * Класс, который сохраняет мир игры в файл
 *
 * Created by Wiliam on 17.09.2017.
 */

public class WorldSaver {
	public static boolean saveWorld(int[][] tiledMap) {
		try {
			PrintWriter writer = new PrintWriter("resources/worlds/the-file-name.txt", "UTF-8");
			int width = tiledMap.length;
			int height = tiledMap[0].length;

			writer.println(width + " " + height);

			
			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					writer.print(tiledMap[y][x]);
					writer.print(x == width - 1 ? "" : " ");
				}
				writer.println();
			}
			writer.close();

		} catch (IOException e) {
			return false;
		}
		return true;
	}
}
