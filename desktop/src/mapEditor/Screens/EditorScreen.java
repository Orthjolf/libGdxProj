package mapEditor.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;

import mapEditor.Input.EditorInput;
import mapEditor.MapEditor;
import mapEditor.Renderer;
import mapEditor.ui.Ui;


public class EditorScreen implements Screen {

	public static int height = 1080;
	public static int width = 1920;
	public int[][] tiledMap = new int[20][20];
	public OrthographicCamera camera;
	public EditorInput editorInput;
	public Stage stage;
	private Box2DDebugRenderer b2dr;
	private AssetManager assets;
	private MapEditor app;
	private Renderer renderer;
	private Batch batch;
	private Ui ui;

	public EditorScreen() {
		b2dr = new Box2DDebugRenderer();
		stage = new Stage();
		app = MapEditor.get();
		camera = app.camera;
		assets = app.assets;
		batch = app.batch;
		editorInput = new EditorInput(this);
		renderer = new Renderer(this);
		camera.position.set(0, 0, 0);
		InputMultiplexer inputMultiplexer = new InputMultiplexer();
		inputMultiplexer.addProcessor(stage);
		inputMultiplexer.addProcessor(editorInput);
		Gdx.input.setInputProcessor(inputMultiplexer);
	}


	@Override
	public void show() {
		ui = new Ui(this);
	}


	@Override
	public void render(float delta) {
		update(delta);
		Gdx.gl.glClearColor(.2f, .2f, .2f, 1);
		Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		{
			batch.setProjectionMatrix(camera.combined);
			renderer.render(batch);
		}
		batch.end();
		ui.draw();
	}


	private void update(float delta) {
		editorInput.update();
		ui.update();
		camera.update();
	}


	@Override
	public void resize(int width, int height) {
		camera.position.x = width / 2f;
		camera.position.y = height / 2f;
		camera.viewportWidth = width;
		camera.viewportHeight = height;
		EditorScreen.height = height;
		EditorScreen.width = width;
		camera.update();
		ui.resize(width,height);
	}


	@Override
	public void hide() {
		this.dispose();
	}


	@Override
	public void dispose() {
		b2dr.dispose();
	}


	@Override
	public void pause() {
	}


	@Override
	public void resume() {

	}
}
