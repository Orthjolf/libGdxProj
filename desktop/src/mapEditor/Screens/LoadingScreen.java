package mapEditor.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;

import java.io.File;

import mapEditor.MapEditor;


/**
 * Загрузочный экран
 *
 * Created by Wiliam on 15.10.2017.
 */
public class LoadingScreen implements Screen {
	private final MapEditor app;

	private ShapeRenderer shapeRenderer;
	private float progress;
	private boolean screenHasChanged;

	public LoadingScreen() {
		app = MapEditor.get();
		this.shapeRenderer = new ShapeRenderer();
	}


	/**
	 * Загрузка ресурсов
	 */
	private void queueAssets() {
		app.assets.load("ui\\uiskin.atlas", TextureAtlas.class);

		File f = new File(System.getProperty("user.dir") + "\\dev");
		for (String s : f.list()) {
			app.assets.load("dev\\" + s, Texture.class);
		}
	}


	@Override
	public void show() {
		screenHasChanged = false;
		progress = 0f;
		queueAssets();
	}


	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0f, 0f, 0f, 0f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		update(delta);
		shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
		shapeRenderer.setColor(Color.WHITE);
		shapeRenderer.rect(100, 100, 1700, 16);

		shapeRenderer.setColor(Color.BLUE);
		shapeRenderer.rect(100, 100, progress * 1700, 16);
		shapeRenderer.end();
	}


	/**
	 * Обновляем прогресс бар. Если загрузка завершена, вызываем новый экран
	 *
	 * @param delta
	 */
	private void update(float delta) {
		if(screenHasChanged)
			return;

		progress = MathUtils.lerp(progress, app.assets.getProgress(), .1f);
		if (app.assets.update() && progress >= app.assets.getProgress() - .001f) {
			app.callScreen(app.editorScreen);
			screenHasChanged = true;
		}
	}


	@Override
	public void resize(int width, int height) {
	}


	@Override
	public void pause() {
	}


	@Override
	public void resume() {
	}


	@Override
	public void hide() {
	}


	@Override
	public void dispose() {
		shapeRenderer.dispose();
	}
}
