package com.mygdx.game.desktop;


import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import mapEditor.MapEditor;


/**
 * Created by Wiliam on 17.08.2017.
 */

public class MapEditorLauncher {

	public static void main(String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.height = 1080;
		config.width = 1920;
		config.allowSoftwareMode = true;
		new LwjglApplication(new MapEditor().get(), config);
	}
}
