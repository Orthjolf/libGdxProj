package com.mygdx.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mygdx.game.JPlatformerGame;

/**
 * Лаунчер ПК версии. Вызывается при запуске игры на компуктере
 *
 * Created by Wiliam on 17.08.2017.
 */
public class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        new LwjglApplication(new JPlatformerGame().get(), config);
    }
}
